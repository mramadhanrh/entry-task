# Black Cat

Black cat is social event sharing platform where user could like, comment, and participate to various event, the project is built with React JS (create-react-app) for the Frontend and Express for the Backend, both Frontend and Backend is using typescript and tslint for linting.

## Setup Guide

1. Clone this repo using `git clone https://gitlab.com/mramadhanr/entry-task.git`
2. Run `npm install` at the root directory of this project. This repository contain 2 project the server and the client, but if you run `npm install` at the root directory both directory dependencies will be installed
3. Run `npm run start` at the root directory
4. You could log in to the app by using email: `user@email.com`, and password: `user123`

## Command

### `npm run test`

Running test using Jest, by executing jest, you could also add arguments for example: `npm run test -- --watch`, running this on root directory will execute both server and client

### `npm run lint`

Lint project using tslint. running this on root directory will execute both server and client

### `npm run build`

Build the project, both project will be placed at build folder, but it's relative to the project directory if it's client it will be at `client/build` and server at `./build`, running this on root directory will execute both server and client

## Technical Design Document

Please [click here to view Technical Design Document](https://gitlab.com/mramadhanr/entry-task/-/blob/master/docs/Technical_Design_Document.md)

## API Design

Please [click here to go to API Design Documentation directory](https://gitlab.com/mramadhanr/entry-task/-/tree/master/docs/api)

## Product Requirements

- [x] Visitors need to login first to view events.
- [x] Visitors can browse a list of events, can filter by date ranges and channels (refer to design) [i.e. event - [x] tags/categories whatever you name it].
- [x] You must implement the infinite-scroll behavior of the event list, meaning as the viewer scroll, the list should grow naturally without user-noticing.
- [x] Visitors can view event details, including title, descriptions, event photos, event date and location, people who has participated in, people who liked, and comments. He or she can comment, like and participate in an event as well.
- [x] You need to follow design specifications provided faithfully. For further details please check out Assets section.

## Git

- [x] **Must** create a git repo on gitlab
- [x] Commit frequently
- [x] Commit message should be meaningful
      (bonus) set up gitlab CI checks for the linting, testing job

## API Design

- [x] **Must** design and document backend APIs to retrieve all necessary data for FE user interactions like login, browsing etc. [Refer here.](https://gitlab.com/mramadhanr/entry-task/-/tree/master/docs/api)
- [x] Decide how you are going to implement session, using sessionid vs token based method. (I use token based method)
- [x] Create your own mock data using faker.js. and serve your fake data using a simple express.js service
- [x] (bonus) Implement session logic on express server (I do add expire time at the JWT token, and verify it on each request except at login routes)

## Dev and Build

- [ ] Cannot use any boilerplate generator like `create-react-app` (I am using `create-react-app`, because it's permitted)
- [x] **Must** use `webpack` as your build tool.
      Should have separate configurations for local development and production build. (default from `create-react-app`)
- [x] **Must** support JavaScript code splitting for your webpack production build. (default from `create-react-app`)
- [x] Have hot reload for your local development.
      (bonus) Support CSS coding splitting. (default from `create-react-app`)
- [x] (bonus) Setup `react-hot-loader for` your local development. (default from `create-react-app`)

## JavaScript/Flow (I am using Typescript)

- [x] Use modern ECMAScript syntax like class, module etc (I am using latest typescript feature)
- [x] Use async/await, generator functions to avoid the callback hell
- [x] Setup babel to transpile newer ECMAScript code for older browsers (default from `create-react-app`)
- [x] **Must** use FlowType as a static type checker and write Flow definition to annotate the code properly (I am not using Flow type but I am using typescript typechecking)
- [ ] Document down API schema with Flow
- [ ] (bonus) Study the min browser version you plan to support and only include the necessary polyfills to save bundle size

## CSS

- [x] **Must** use SCSS as preprocessor
- [x] **Must** use CSS Modules with your React component (I use Sass module)
- [ ] (bonus) Add autoPrefixer to cover browser discrepancies on CSS implementation

## React

- [x] **Must** use React 16+ for the entry task
- [x] Use `react-router` for the routing
- [x] (bonus) Explore using newer React APIs like hooks, suspense, memo

## Redux

- [x] **Must** use `redux` to manage your data logic
- [x] **Must** use `redux-thunk` for async actions
- [ ] (bonus) Support inject reducer asynchronously on demand instead of combine all the reducers up front

## Linting

- [x] Setup ESLint as a `pre-commit` hook (I use tslint as precommit hook)
- [x] Use `prettier` to format your code automatically
- [ ] Use `stylelint` as a `pre-commit` hook to lint your CSS code as well

## Testing

- [x] Set up `jest` and `enzyme` as your test framework
- [x] Write at least one snapshot test
- [x] Use enzyme to test out the rendering logic for at least one React component

## Bonus

- [ ] Support internationalisation (i18n)
- [ ] Support server side rendering
- [ ] Annotate page properly with SEO meta tags

## Deliverables

- [x] [Project Source Code](https://gitlab.com/mramadhanr/entry-task) 
- [x] [Project Setup Guide](https://gitlab.com/mramadhanr/entry-task#setup-guide)
- [x] [Tech Design Document](https://gitlab.com/mramadhanr/entry-task/-/blob/master/docs/Technical_Design_Document.md)
- [x] [Checklist filled for the tech requirements mentioned above](https://gitlab.com/mramadhanr/entry-task#product-requirements)
