import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { getUserSession } from '../helpers/fakeDataCache/session';
import { JWTValue } from '../common/types';

export const verifyAuthSession = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const userToken: string = req.headers['user-token'] as string;
  const sessions = getUserSession();
  const sendUnautorized = () => {
    res.status(403);
    res.json({
      status: 403,
      message: 'Unauthorized access',
    });
  };

  if (sessions[userToken] === undefined) {
    sendUnautorized();
    return;
  }

  const { exp } = jwt.decode(sessions[userToken]) as JWTValue;
  if (!exp) {
    sendUnautorized();
    return;
  }

  if (+new Date() >= exp) {
    res.status(401);
    res.json({
      status: 401,
      message: 'Token is expired',
    });
    return;
  }

  next();
};
