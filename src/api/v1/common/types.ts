export interface Channel {
  id: number;
  name: string;
}

export interface User {
  id: number;
  nickname: string;
  fullname: string;
  picture: string;
  email: string;
  password: string;
}

export type ActivityDateEnd = number | 'NOT_SET';

export interface ActivityLocation {
  city: string;
  street: string;
  latitude: string | number;
  longitude: string | number;
}

export interface ActivityComment {
  id: number;
  user: User;
  text: string;
  dateComment: number | string;
}

export interface Activity {
  id: number;
  author: User;
  channel: Channel;
  title: string;
  content: string;
  imageCollection: string[];
  thumbnail: string;
  datePosted: number;
  dateStart: number;
  dateEnd: ActivityDateEnd;
  userLike: User[];
  userGoing: User[];
  isGoing: boolean;
  isLike: boolean;
  location: ActivityLocation;
  commentCollection: ActivityComment[];
}

export interface ListById<T> {
  [id: string]: T;
}

export interface FakeDataCache {
  channels: ListById<Channel>;
  users: ListById<User>;
  activity: ListById<Activity>;
}

export interface ActivityFeedQuery {
  limit: number;
  offset: number;
}

export interface ActivityQuery {
  id: number;
}

export interface ActivityFeedResponse {
  results: ListById<Activity>;
  next: string | null;
  previous: string | null;
}

export interface JWTValue {
  id: number;
  exp: number;
  name: string;
  iat: number;
}
