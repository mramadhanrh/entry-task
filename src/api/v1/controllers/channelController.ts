import { Request, Response } from 'express';
import { getFakeDataCache } from '../helpers/fakeDataCache';

const get = (req: Request, res: Response) => {
  const { channels } = getFakeDataCache();

  res.json({
    channels: Object.values(channels),
  });
};

export default {
  get,
};
