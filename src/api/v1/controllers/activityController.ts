import { Request, Response } from 'express';
import {
  getActivityFeedPage,
  getActivityByChannelAndTime,
} from '../helpers/fakeDataCache/activity';
import {
  ActivityFeedResponse,
  ActivityFeedQuery,
  ActivityComment,
  ActivityQuery,
  ListById,
  Activity,
  User,
} from '../common/types';
import { getFakeDataCache, setFakeDataCache } from '../helpers/fakeDataCache';

const get = (req: Request, res: Response) => {
  const { query }: { query: ActivityQuery } = req;
  const { activity } = getFakeDataCache();

  res.json(activity[query.id]);
};

const postLike = (req: Request, res: Response) => {
  const { query } = req;
  const { id, 'user-id': userId } = query;
  const { activity, users } = getFakeDataCache();
  let newLikeData = [...activity[id].userLike];
  if (activity[id].isLike) {
    newLikeData = newLikeData.filter((user: User) => {
      return user.id !== Number(userId);
    });
  } else {
    newLikeData.push(users[userId]);
  }

  setFakeDataCache<ListById<Activity>>('activity', {
    ...activity,
    [id]: {
      ...activity[id],
      isLike: !activity[id].isLike,
      userLike: newLikeData,
    },
  });
  res.json({
    message: 'Successfully changed',
  });
};

const postGoing = (req: Request, res: Response) => {
  const { query } = req;
  const { id, 'user-id': userId } = query;
  const { activity, users } = getFakeDataCache();
  let newGoingData = [...activity[id].userGoing];

  if (activity[id].isGoing) {
    newGoingData = newGoingData.filter((user: User) => {
      return user.id !== Number(userId);
    });
  } else {
    newGoingData.push(users[userId]);
  }

  setFakeDataCache<ListById<Activity>>('activity', {
    ...activity,
    [id]: {
      ...activity[id],
      isGoing: !activity[id].isGoing,
      userGoing: newGoingData,
    },
  });
  res.json({
    message: 'Successfully changed',
  });
};

const postComment = (req: Request, res: Response) => {
  const { query, body } = req;
  const { id, 'user-id': userId } = query;
  const { text } = body;
  const { activity, users } = getFakeDataCache();

  setFakeDataCache<ListById<Activity>>('activity', {
    ...activity,
    [id]: {
      ...activity[id],
      commentCollection: [
        ...activity[id].commentCollection,
        {
          id: activity[id].commentCollection.length,
          dateComment: +new Date(),
          text,
          user: users[userId],
        } as ActivityComment,
      ],
    },
  });

  res.json({
    result: [
      ...activity[id].commentCollection,
      {
        id: activity[id].commentCollection.length,
        dateComment: +new Date(),
        text,
        user: users[userId],
      } as ActivityComment,
    ],
    message: 'Successfully changed',
  });
};

const getFeed = (req: Request, res: Response) => {
  const { query }: { query: ActivityFeedQuery } = req;

  const activityPage: ActivityFeedResponse = getActivityFeedPage(
    query.limit,
    query.offset
  );

  res.json(activityPage);
};

const getSearchFeed = (req: Request, res: Response) => {
  const { query } = req;
  const { channel, timestart, timeend } = query;

  res.json({
    results: getActivityByChannelAndTime(timestart, timeend, channel),
  });
};

export default {
  get,
  like: {
    post: postLike,
  },
  going: {
    post: postGoing,
  },
  feed: {
    get: getFeed,
  },
  comment: {
    post: postComment,
  },
  searchFeed: {
    get: getSearchFeed,
  },
};
