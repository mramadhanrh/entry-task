import { Request, Response } from 'express';
import { size, omit } from 'lodash';
import { getFakeDataCache } from '../helpers/fakeDataCache';
import { createSessionToken } from '../helpers/fakeDataCache/session';

const post = (req: Request, res: Response) => {
  const { body } = req;
  const { email, password } = body;
  const { users } = getFakeDataCache();
  const { id, email: uemail, password: upass } = users[size(users)];

  if (email === uemail && password === upass) {
    const token = createSessionToken(id);
    res.status(200);
    res.json({
      token,
      data: omit(users[size(users)], ['email', 'password']),
      message: 'Successfully Logged In',
    });
    return;
  }

  res.status(403);
  res.json({
    message: 'Unauthorized Forbidden Access',
  });
};

export default {
  post,
};
