import * as faker from 'faker';

export const getRandomArrayElement = <T>(arr: T[]): T => {
  return arr[Math.floor(Math.random() * arr.length)];
};

/**
 *
 * @param min Minimum value of random number
 * @param max Maximum value of random number, the maximum value will be not included
 * @param floor Floor the value as the same as Math.floor
 */
export const getRandomNumberInRange = (
  min: number,
  max: number,
  floor: boolean = true
): number => {
  const rand = Math.random() * (max - min) + min;
  if (floor) {
    return Math.floor(rand);
  }
  return rand;
};

export const getRandomImage = (
  width: number = 500,
  height: number = 500,
  source: string = `https://source.unsplash.com/random`,
  randomize: boolean = true
): string => {
  return `${source}/${width}x${height}?${
    randomize ? faker.random.number(500) : ''
  }`;
  // return faker.image.imageUrl(width, height, undefined, true);
};
