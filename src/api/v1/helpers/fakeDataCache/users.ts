import * as faker from 'faker';
import { size } from 'lodash';
import { User, ListById } from '../../common/types';
import { getDataList, getFakeDataCache } from '.';
import { getRandomImage } from '../randomHelper';

export const generateUsers = (length: number = 0): ListById<User> => {
  return {
    ...getDataList<User>((id) => {
      const firstName = faker.name.firstName();
      const lastName = faker.name.lastName();
      return {
        id,
        nickname: firstName.toLowerCase(),
        fullname: `${firstName} ${lastName}`,
        picture: getRandomImage(),
        email: faker.internet.email(firstName, lastName),
        password: 'user123',
      };
    }, length),
    [length + 1]: {
      id: length + 1,
      nickname: 'usersample',
      fullname: 'User Example',
      picture:
        'https://images.unsplash.com/photo-1555952517-2e8e729e0b44?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=80',
      email: 'user@email.com',
      password: 'user123',
    },
  };
};

export const getRandomUserFromCache = (): User => {
  const { users } = getFakeDataCache();
  const randomIndex = Math.floor(Math.random() * size(users));
  const { id } = Object.values(users)[randomIndex];

  return users[id];
};
