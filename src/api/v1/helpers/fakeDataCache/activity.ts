import * as faker from 'faker';
import { size, filter } from 'lodash';
import {
  Activity,
  ActivityDateEnd,
  User,
  ListById,
  ActivityFeedResponse,
  ActivityComment,
} from '../../common/types';
import { getDataList, getFakeDataCache } from '.';
import { getRandomUserFromCache } from './users';
import { getRandomChannelFromCache } from './channels';
import {
  getRandomNumberInRange,
  getRandomArrayElement,
  getRandomImage,
} from '../randomHelper';

export const getRandomImageCollection = (
  min: number,
  max: number
): string[] => {
  const imgList: string[] = [];
  const length: number = getRandomNumberInRange(min, max + 1);

  for (let i = 0; i < length; i += 1) {
    imgList.push(getRandomImage());
  }
  return imgList;
};

export const getRandomDateRange = (): {
  dateStart: number;
  dateEnd: ActivityDateEnd;
} => {
  const activityDuration: ActivityDateEnd = getRandomArrayElement<
    ActivityDateEnd
  >(['NOT_SET', getRandomNumberInRange(0, 73)]);

  const dateStart: Date = getRandomArrayElement<Date>([
    faker.date.recent(),
    faker.date.future(),
  ]);
  const dateEnd: ActivityDateEnd =
    activityDuration !== 'NOT_SET'
      ? +new Date(
          new Date(dateStart).setHours(dateStart.getHours() + activityDuration)
        )
      : 'NOT_SET';

  return {
    dateStart: +dateStart,
    dateEnd,
  };
};

export const getRandomUserCollection = (min: number, max: number): User[] => {
  const userCollection: User[] = [];
  const { users } = getFakeDataCache();
  const randRange: number = getRandomNumberInRange(
    min,
    size(users) <= max ? size(users) : max
  );

  for (let i = 0; i < randRange; i += 1) {
    userCollection.push(users[i]);
  }

  return userCollection;
};

const getRandomActivityComment = (length: number = 10): ActivityComment[] => {
  const comments: ActivityComment[] = [];
  const { users } = getFakeDataCache();

  for (let i = 0; i < length; i += 1) {
    comments.push({
      id: i,
      text: faker.lorem.lines(),
      dateComment: +faker.date.past(),
      user: users[Math.floor(Math.random() * size(users))],
    });
  }

  return comments;
};

export const generateActivity = (length: number = 0): ListById<Activity> => {
  return getDataList<Activity>((id) => {
    const { dateStart, dateEnd } = getRandomDateRange();
    return {
      id,
      author: getRandomUserFromCache(),
      channel: getRandomChannelFromCache(),
      title: faker.lorem.lines(),
      content: faker.lorem.paragraphs(10, '<br/><br/>'),
      thumbnail: getRandomImage(64, 64),
      imageCollection: getRandomImageCollection(3, 3),
      datePosted: +faker.date.past(),
      dateStart,
      dateEnd,
      userLike: getRandomUserCollection(20, 50),
      userGoing: getRandomUserCollection(20, 50),
      isGoing: false,
      isLike: false,
      location: {
        city: faker.address.city(),
        street: `${faker.address.streetAddress()}, ${faker.address.secondaryAddress()}`,
        latitude: faker.address.latitude(),
        longitude: faker.address.longitude(),
      },
      commentCollection: getRandomActivityComment(
        Math.floor(Math.random() * 20)
      ),
    };
  }, length);
};

export const getActivityFeedPage = (
  limit: number = 20,
  offset: number = 0
): ActivityFeedResponse => {
  const { activity } = getFakeDataCache();
  const activityCollection: Activity[] = Object.values(activity);
  const offsetNumber: number = Number(offset) || 0;
  const limitNumber: number = Number(limit) || 0;
  const end: number = offsetNumber + limitNumber;

  const nextOffset: number | null =
    end > activityCollection.length ? activityCollection.length : end;
  const previous = (offsetNumber || activityCollection.length) - limitNumber;
  const previousOffset: number | null = previous < 0 ? null : previous;

  const slicedActivity = activityCollection.slice(
    offsetNumber,
    offsetNumber + limitNumber
  );

  const slicedActObj = slicedActivity.reduce(
    (obj: ListById<Activity>, item: Activity) => {
      return {
        ...obj,
        [item.id]: item,
      };
    },
    {}
  );

  return {
    results: slicedActObj,
    next: `?offset=${nextOffset}&limit=${limit}`,
    previous: previousOffset ? `?offset=${previousOffset}&limit${limit}` : null,
  };
};

export const getActivityByChannelAndTime = (
  from?: number,
  to?: number,
  channel?: string
) => {
  const { activity } = getFakeDataCache();

  return filter(activity, (obj: Activity) => {
    const isSameChannel = !channel || obj.channel.name === channel;
    const isFrom = !from || obj.dateStart >= from;
    const isTo = !to || obj.dateStart <= to;

    return isSameChannel && isFrom && isTo;
  }).reduce((obj, item) => {
    return {
      ...obj,
      [item.id]: item,
    };
  }, {});
};
