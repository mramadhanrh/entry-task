import {
  FakeDataCache,
  Channel,
  User,
  Activity,
  ListById,
} from '../../common/types';
import { generateChannels } from './channels';
import { generateUsers } from './users';
import { generateActivity } from './activity';

let cache: FakeDataCache = {
  channels: {},
  users: {},
  activity: {},
};

export const setFakeDataCache = <T>(key: keyof FakeDataCache, obj: T): void => {
  cache = {
    ...cache,
    [key]: obj,
  };
};

export const getFakeDataCache = (): FakeDataCache => {
  return cache;
};

export const getDataList = <T>(
  valueGenerator: (index: number) => T,
  length: number = 0
) => {
  const data: { [id: string]: T } = {};
  for (let i = 0; i < length; i += 1) {
    data[i] = valueGenerator(i);
  }
  return data;
};

export const initializeFakeDataCache = (): FakeDataCache => {
  setFakeDataCache<ListById<Channel>>('channels', generateChannels(10));
  setFakeDataCache<ListById<User>>('users', generateUsers(50));
  setFakeDataCache<ListById<Activity>>('activity', generateActivity(100));

  return getFakeDataCache();
};
