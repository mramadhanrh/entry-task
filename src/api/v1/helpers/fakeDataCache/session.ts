import * as jwt from 'jsonwebtoken';
import { getFakeDataCache } from '.';
import { ListById } from '../../common/types';

let userSessions: ListById<string> = {};

export const setUserSession = (newSession: string): void => {
  userSessions = { ...userSessions, [newSession]: newSession };
};

export const getUserSession = (): ListById<string> => userSessions;

export const createSessionToken = (id: number): string => {
  const { users } = getFakeDataCache();
  const exp: Date = new Date();
  const days: number = 30;
  // Get unix milliseconds at current time plus number of days
  exp.setTime(+exp + days * 86400000); // 24 * 60 * 60 * 1000
  const token = jwt.sign(
    { id, exp: +exp, name: users[id].nickname },
    'private',
    {
      algorithm: 'HS256',
    }
  );

  setUserSession(token);

  return token;
};
