import * as faker from 'faker';
import { size } from 'lodash';
import { getDataList, getFakeDataCache } from '.';
import { Channel, ListById } from '../../common/types';

export const generateChannels = (length: number = 0): ListById<Channel> => {
  return getDataList<Channel>(
    (id) => ({ id, name: faker.commerce.product() }),
    length
  );
};

export const getRandomChannelFromCache = (): Channel => {
  const { channels } = getFakeDataCache();
  const randomIndex = Math.floor(Math.random() * size(channels));
  const { id } = Object.values(channels)[randomIndex];

  return channels[id];
};
