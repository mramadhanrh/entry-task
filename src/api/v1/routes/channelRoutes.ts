import * as express from 'express';
import { channelController } from '../controllers';
import { verifyAuthSession } from '../middlewares/auth';

const router = express.Router();

router.use(verifyAuthSession);
router.get('/', channelController.get);

export default router;
