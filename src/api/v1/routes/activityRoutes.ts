import * as express from 'express';
import * as bodyParser from 'body-parser';
import { activityController } from '../controllers';
import { verifyAuthSession } from '../middlewares/auth';

const router = express.Router();

router.use(verifyAuthSession);

router.use(bodyParser.json());

router.get('/', activityController.get);

router.post('/like', activityController.like.post);
router.post('/going', activityController.going.post);
router.post('/comment', activityController.comment.post);

router.get('/feed', activityController.feed.get);
router.get('/search', activityController.searchFeed.get);

export default router;
