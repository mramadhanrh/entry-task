/**
 * Export aliases used as key that define routes
 * for example export { default as auth } from './x/y;
 * will be '[prefix]/auth' routes
 * you could just change the export aliases
 * if you would like to change the routes path
 */

export { default as activity } from './activityRoutes';
export { default as channel } from './channelRoutes';
export { default as login } from './loginRoutes';
