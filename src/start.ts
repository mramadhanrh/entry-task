import * as express from 'express';
import { Request, Response, IRouter } from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';

import * as routes from './api/v1/routes';
import {
  initializeFakeDataCache,
  getFakeDataCache,
} from './api/v1/helpers/fakeDataCache';

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, '..', 'client', 'build')));

for (const name of Object.keys(routes)) {
  app.use(`/api/v1/${name}`, (routes as any)[name] as IRouter);
}

initializeFakeDataCache();

app.get('/api/v1/checkfakedata', (req: Request, res: Response) => {
  return res.send(getFakeDataCache());
});

app.get('/', (req: Request, res: Response) => {
  res.sendfile(path.join(__dirname, '..', 'client', 'build', 'index.html'));
});

app.listen(process.env.PORT || 8080);
