# API Design

### GET `/api/v1/activity`
Receive Activity details data by giving the activity detail id.

**Request**

**Params**:
- id: number

**Response**
```
{
    "id": 0,
    "author": {
        "id": 11,
        "nickname": "diamond",
        "fullname": "Diamond Green",
        "picture": "https://source.unsplash.com/random/500x500?230",
        "email": "Diamond.Green46@hotmail.com",
        "password": "user123"
    },
    "channel": {
        "id": 8,
        "name": "Sausages"
    },
    "title": "Maxime et dolorum est consequatur in.",
    "content": "Voluptates modi necessitatibus assumenda. V...",
    "thumbnail": "https://source.unsplash.com/random/64x64?93",
    "imageCollection": [
        "https://source.unsplash.com/random/500x500?26",
        "https://source.unsplash.com/random/500x500?96",
        "https://source.unsplash.com/random/500x500?169"
    ],
    "datePosted": 1556951829168,
    "dateStart": 1591762086237,
    "dateEnd": 1591862886237,
    "userLike": [
	    ...
        {
            "id": 0,
            "nickname": "hillary",
            "fullname": "Hillary Spinka",
            "picture": "https://source.unsplash.com/random/500x500?182",
            "email": "Hillary.Spinka24@gmail.com",
            "password": "user123"
        }
        ...
    ],
    "userGoing": [
	    ...
        {
            "id": 0,
            "nickname": "hillary",
            "fullname": "Hillary Spinka",
            "picture": "https://source.unsplash.com/random/500x500?182",
            "email": "Hillary.Spinka24@gmail.com",
            "password": "user123"
        }
        ...
    ],
    "isGoing": false,
    "isLike": false,
    "location": {
        "city": "South Malikafurt",
        "street": "17636 Julio Parkway, Apt. 273",
        "latitude": "59.7181",
        "longitude": "-144.9649"
    },
    "commentCollection": [
	    ...
        {
            "id": 0,
            "text": "Et voluptas ut neque suscipit ut quod nam.",
            "dateComment": 1574337056089,
            "user": {
                "id": 33,
                "nickname": "keagan",
                "fullname": "Keagan Emard",
                "picture": "https://source.unsplash.com/random/500x500?334",
                "email": "Keagan_Emard86@hotmail.com",
                "password": "user123"
            }
        },
        ...
    ]
}
```

### GET `/api/v1/activity/feed`

Receive list of activity for activity feed

**Request**

**Params**:
- offset: number
- limit: number

**Response**
```
		"results":{ 
			0: {
			    "id": 0,
			    "author": {
			        "id": 11,
			        "nickname": "diamond",
			        "fullname": "Diamond Green",
			        "picture": "https://source.unsplash.com/random/500x500?230",
			        "email": "Diamond.Green46@hotmail.com",
			        "password": "user123"
			    },
			    "channel": {
			        "id": 8,
			        "name": "Sausages"
			    },
			    "title": "Maxime et dolorum est consequatur in.",
			    "content": "Voluptates modi necessitatibus assumenda. V...",
			    "thumbnail": "https://source.unsplash.com/random/64x64?93",
			    "imageCollection": [
			        "https://source.unsplash.com/random/500x500?26",
			        "https://source.unsplash.com/random/500x500?96",
			        "https://source.unsplash.com/random/500x500?169"
			    ],
			    "datePosted": 1556951829168,
			    "dateStart": 1591762086237,
			    "dateEnd": 1591862886237,
			    "userLike": [
				    ...
			        {
			            "id": 0,
			            "nickname": "hillary",
			            "fullname": "Hillary Spinka",
			            "picture": "https://source.unsplash.com/random/500x500?182",
			            "email": "Hillary.Spinka24@gmail.com",
			            "password": "user123"
			        }
			        ...
			    ],
			    "userGoing": [
				    ...
			        {
			            "id": 0,
			            "nickname": "hillary",
			            "fullname": "Hillary Spinka",
			            "picture": "https://source.unsplash.com/random/500x500?182",
			            "email": "Hillary.Spinka24@gmail.com",
			            "password": "user123"
			        }
			        ...
			    ],
			    "isGoing": false,
			    "isLike": false,
			    "location": {
			        "city": "South Malikafurt",
			        "street": "17636 Julio Parkway, Apt. 273",
			        "latitude": "59.7181",
			        "longitude": "-144.9649"
			    },
			    "commentCollection": [
				    ...
			        {
			            "id": 0,
			            "text": "Et voluptas ut neque suscipit ut quod nam.",
			            "dateComment": 1574337056089,
			            "user": {
			                "id": 33,
			                "nickname": "keagan",
			                "fullname": "Keagan Emard",
			                "picture": "https://source.unsplash.com/random/500x500?334",
			                "email": "Keagan_Emard86@hotmail.com",
			                "password": "user123"
			            }
			        },
			        ...
			    ]
			}
			...
		},
		next: "?offset=20&limit=20",
		previous: "?offset=80&limit20"
```

### POST `/api/v1/activity/like`

Change like value at the server by requesting to this routes

**Request**

**Params**:
- id: number

**Response**

```
{
	message: 'Successfully changed',
}
```

### POST `/api/v1/activity/going`

Change going value at the server by requesting to this routes

**Request**

**Params**:
- id: number

**Response**

```
{
	message: 'Successfully changed',
}
```


### GET`/api/v1/activity/search`

Receive list of activity by giving specified channel and timestamp value for timestart and timeend params

**Request**

**Params**:
- channel: string
- timestart: number,
- timeend: number

**Response**
```
		"results":{ 
			0: {
			    "id": 0,
			    "author": {
			        "id": 11,
			        "nickname": "diamond",
			        "fullname": "Diamond Green",
			        "picture": "https://source.unsplash.com/random/500x500?230",
			        "email": "Diamond.Green46@hotmail.com",
			        "password": "user123"
			    },
			    "channel": {
			        "id": 8,
			        "name": "Sausages"
			    },
			    "title": "Maxime et dolorum est consequatur in.",
			    "content": "Voluptates modi necessitatibus assumenda. V...",
			    "thumbnail": "https://source.unsplash.com/random/64x64?93",
			    "imageCollection": [
			        "https://source.unsplash.com/random/500x500?26",
			        "https://source.unsplash.com/random/500x500?96",
			        "https://source.unsplash.com/random/500x500?169"
			    ],
			    "datePosted": 1556951829168,
			    "dateStart": 1591762086237,
			    "dateEnd": 1591862886237,
			    "userLike": [
				    ...
			        {
			            "id": 0,
			            "nickname": "hillary",
			            "fullname": "Hillary Spinka",
			            "picture": "https://source.unsplash.com/random/500x500?182",
			            "email": "Hillary.Spinka24@gmail.com",
			            "password": "user123"
			        }
			        ...
			    ],
			    "userGoing": [
				    ...
			        {
			            "id": 0,
			            "nickname": "hillary",
			            "fullname": "Hillary Spinka",
			            "picture": "https://source.unsplash.com/random/500x500?182",
			            "email": "Hillary.Spinka24@gmail.com",
			            "password": "user123"
			        }
			        ...
			    ],
			    "isGoing": false,
			    "isLike": false,
			    "location": {
			        "city": "South Malikafurt",
			        "street": "17636 Julio Parkway, Apt. 273",
			        "latitude": "59.7181",
			        "longitude": "-144.9649"
			    },
			    "commentCollection": [
				    ...
			        {
			            "id": 0,
			            "text": "Et voluptas ut neque suscipit ut quod nam.",
			            "dateComment": 1574337056089,
			            "user": {
			                "id": 33,
			                "nickname": "keagan",
			                "fullname": "Keagan Emard",
			                "picture": "https://source.unsplash.com/random/500x500?334",
			                "email": "Keagan_Emard86@hotmail.com",
			                "password": "user123"
			            }
			        },
			        ...
			    ]
			}
			...
		},
```