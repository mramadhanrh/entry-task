# API Design

### GET `/api/v1/login`
Auth user by giving email and user password at the body

**Response**
```
{
    "data": {
        "id": 51,
        "nickname": "usersample",
        "fullname": "User Example",
        "picture": "https://images.unsplash.com/photo-1555952517-2e8e729e0b44?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=80"
    },
    "message": "Successfully Logged In"
}
```