# API Design

### GET `/api/v1/channel`
Receive all channel by requesting to this routes

**Response**
```
{
    "channels": [
        {
            "id": 0,
            "name": "Ball"
        },
        ...
    ]
}
```