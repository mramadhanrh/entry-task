# Technical Design Document

## Introduction
Black Cat is a **social event sharing platform where visitors can view events in various conditions, like and participant their favourite events, and view other people’s activities.**.

## Purpose & Objective
Black Cat is a Web Frontend Task aims to help new developers acquaint themselves with commonly used frontend fundamentals, technologies, and code style.
To test relevant technical skills and learning abilities, as well as to serve as a guide for future job assignments.

## Product Requirement
- Visitors need to login first to view events.
- Visitors can browse a list of events, can filter by date ranges and channels (refer to design) [i.e. event tags/categories whatever you name it].
- You must implement the infinite-scroll behavior of the event list, meaning as the viewer scroll, the list should grow naturally without user-noticing.
- Visitors can view event details, including title, descriptions, event photos, event date and location, people who has participated in, people who liked, and comments. He or she can comment, like and participate in an event as well.
- You need to follow design specifications provided faithfully. For further details please check out Assets section.

## Project Scope
The web app is described as such:
- The user could login with their account or for example in this project you could log in using `email: user@email.com and password: user123`
- User could view list of activity in form of feed after login and could like and plan going to the event by clicking button at feed page
- User could search event by set of time range, or custom time range that they prefer, and channel or all channel if they would
- User could view activity detail, who's going to the activity and who's liking the activity, also who comments to the activity
- User also could comment to the activity at activity detail page
- User could view when is the activity started and ended
- User also could like and plan to go to the activity at the activity detail page

## Technical Overview
Project Configuration

- Gitlab repository link: https://gitlab.com/mramadhanr/entry-task

Tools Used

| Users      | Tools                 | Use                | 
|------------|-----------------------|--------------------|
| Programmer | IDE                   | Visual Studio Code | 
| Programmer | Programming Language  | Typescript         |
| Programmer | UI Library            | React              | 
| Programmer | Server Side Framework | Express            | 

## Technical Detail

### Page diagram
![Page_Diagram](/uploads/4179fc8fa836beb70cb5854c52af9f04/Untitled_Diagram__3_.png)

### Redux
The project is using [redux](https://www.npmjs.com/package/redux), [redux-thunk](https://www.npmjs.com/package/redux-thunk), and [redux-persist](https://www.npmjs.com/package/redux-persist) library for global state management, and also the project have [redux dev tools extension](https://github.com/zalmoxisus/redux-devtools-extension) library to help viewing redux state;

#### **Reducer**
**Activity**: Reducer that contain all data that related to activity
The state is:
- data: where we put activity list to be used at Activity Feed Page
- searchData: contain data that have exact same structure with *data* key reducer but this one is used for search activity at Activity Search Feed Page
- activeData: data that is used by Activity Detail Page
- status: data that used for determining fetch is successful or not for *activeData* key
- isSearch: boolean data that used to determine if user is at search condition or not. (used to determine showing search result or normal feed result)
- nextQuery: string parameter that will be used on every request to activity feed, used for infinite scrolling system.

**Channels**: Reducer that contain channel data list that shown at search sidebar.
The state is:
- data: containing array object that have channel data structure, and used for search sidebar at Activity Feed Page

**Auth**: reducer for saving auth data, the data is persisted and used to authenticate between client and server
The state is:
- user: contain user info and having user data structure that is defined in an Interface.
- token: contain user token that is used as login credential, received at response when creating post request to login
- loginStatus: used to trigger login page if successful to Activity Feed Page or if the token is invalid will be redirected again to Login Page

### Project Structure
The Client project is using atomic design for component structure, but for the server project is just separating between controller, helper, middleware, and routes.

#### Client Project Structure
**src/api**
is where axiosInstance and api routes belong. the axios instance is also using interceptors and defined here.

**src/components**
is where i put react components splitted by atomic design principle, but the pages and templates folder are outside from this folder, it's actually preferrable to put inside the components folder but i would prefer outside from the components folder for ease access to App.tsx and ease access template to pages. and also there's `hoc/` directory to put all Higher Order component, and if there's context or custom hook component they are belong to their directory that is `src/components/context` for context and `src/components/hooks` for custom hooks

**src/constants**
where all constant value belong such as assets path, color scheme, and etc.

**src/helpers**
Helpers folder is used to put all reusable function that is general. such as arrayy or object manipulator and date formatter.

**src/interfaces**
Place to put general data types, if it's for react components use types.ts at their component directory or put it at components scripts. 

**src/pages**
Place where we put page component that will be put at AppRoutes.tsx to define the routes

**src/redux**
Is where we put all redux state reducer, actions, and types, splitted by module name, ex: `src/redux/activity/reducer.ts`

**src/templates**
Templates are page-level objects that place components into a layout and articulate the design’s underlying content structure.

### Server project structure
The server project is grouped by api version, for example for api version 1 is `src/api/v1/` and then the project is inside the directory

**src/api/v1/common**
Is where we put interfaces and other primitive data type.

**src/api/v1/controller**
Controller is place to put all api logic and place to control all the request and response for the api.

**src/api/v1/helpers**
Helpers folder is where we put general function or function that could be reused, but that include with fake data generator

**src/api/v1/middlewares**
Middleware folder to handle request before it's going to controller, the middleware will always be used at routes directory

**src/api/v1/routes**
Where we define all our routes and use the controller and middleware, the routes will be exported as default then re-exported again at index.tsx at the same directory, the export aliases at index.tsx will be their routes path, for example `export { default as activity } from './activity'` will be `/api/v1/activity` routes