import { getReadableDateFormat } from '../dateHelper';

describe('dateHelper', () => {
  describe('getReadableDateFormat', () => {
    it('Receive expected format', () => {
      expect(getReadableDateFormat(new Date(0))).toBe('1 Jan 1970 07:00');
    });

    it('Only give the date', () => {
      expect(getReadableDateFormat(new Date(0), true)).toBe('1 Jan 1970 ');
    });

    it('Only give the hour', () => {
      expect(getReadableDateFormat(new Date(0), false, true)).toBe(' 07:00');
    });
  });
});
