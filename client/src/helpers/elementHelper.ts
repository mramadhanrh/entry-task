export const getElementPosition = (
  el: HTMLElement
): { x: number | undefined; y: number | undefined } => {
  if (!el) return { x: undefined, y: undefined };
  const { offsetTop, offsetLeft } = el;

  return {
    x: offsetLeft,
    y: offsetTop,
  };
};

export const isOverElement = (
  el: HTMLElement | null,
  scrollTop: number
): boolean => {
  if (!el) return false;
  const { offsetTop } = el;
  return scrollTop >= offsetTop;
};
