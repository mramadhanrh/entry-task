import { DateCategory } from '../components/organisms/DateCategoryList/DateCategoryList';

export const formatAMPM = (date: Date) => {
  let hours: string | number = date.getHours();
  let minutes: string | number = date.getMinutes();
  const ampm: string = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? `0 ${minutes}` : minutes;
  return {
    ampm,
    hours: `${hours}:${minutes}`,
  };
};

export const getReadableDateFormat = (
  date: Date,
  hideHour: boolean = false,
  hideDate: boolean = false
): string => {
  const dateMonth = date.getDate();
  const month = date.toLocaleDateString('default', { month: 'short' });
  const year = date.getFullYear();
  const hours = date
    .getHours()
    .toLocaleString('default', { minimumIntegerDigits: 2 });
  const minutes = date
    .getMinutes()
    .toLocaleString('default', { minimumIntegerDigits: 2 });

  const dateText = hideDate ? '' : `${dateMonth} ${month} ${year}`;
  const hourText = hideHour ? '' : `${hours}:${minutes}`;

  return `${dateText} ${hourText}`;
};

export const getPastDateRange = (from: Date, to: Date = new Date()): string => {
  const diff: number = Math.floor(to.getTime() - from.getTime());
  const day: number = 1000 * 60 * 60 * 24;

  const days: number = Math.floor(diff / day);
  const months: number = days / 30;
  const years: number = months / 12;

  if (years >= 1) return `${Math.floor(years)} years ago`;
  if (months >= 1) return `${Math.floor(months)} months ago`;

  return `${days} days ago`;
};

export const getCopywritingDateRange = (from?: number, to?: number): string => {
  const fromText = from ? getReadableDateFormat(new Date(from), true) : '';
  const toText = to ? getReadableDateFormat(new Date(to), true) : '';
  return `${fromText ? `from ${fromText}` : ''} ${
    toText ? `to ${toText}` : ''
  }`;
};

export const getCategoryRange = (
  category: DateCategory
): { from: Date; by: number } => {
  switch (category) {
    case 'today':
      return {
        from: new Date(),
        by: 0,
      };
    case 'tomorrow':
      return {
        from: new Date(new Date().setDate(new Date().getDate() + 1)),
        by: 0,
      };
    case 'this week':
      return {
        from: new Date(),
        by: 7,
      };
    default:
      return {
        from: new Date(),
        by: 30,
      };
  }
};

export const getDateRange = (
  from: Date,
  by: number
): { from: Date; to: Date } => {
  if (by === 0) {
    const dayFrom = new Date(from);
    const dayTo = new Date(from);
    dayFrom.setHours(0, 0, 0);
    dayTo.setHours(23, 59, 59);
    return {
      from: dayFrom,
      to: dayTo,
    };
  }

  const to = new Date(from);
  to.setDate(to.getDate() + by);

  return {
    from,
    to,
  };
};
