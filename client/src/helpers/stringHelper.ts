export const limitString = (
  str: string,
  limit: number = -1,
  elipsis: boolean = false
): string => {
  if (limit < 0) return str;

  const strElipsis = elipsis ? '...' : '';
  const slicedStr = str.slice(0, limit);
  const isSliced = str.length > limit;
  return isSliced ? `${slicedStr}${strElipsis}` : slicedStr;
};

export const escapeHtml = (str) => {
  return str
    .replace(/<style[^>]*>.*<\/style>/gm, '')
    .replace(/<[^>]+>/gm, '')
    .replace(/([\r\n]+ +)+/gm, '');
};
