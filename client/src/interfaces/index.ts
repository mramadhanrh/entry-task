import { AxiosInstance } from 'axios';

export type ApiInstance = () => AxiosInstance;

export interface ReduxAction<T, K> {
  type: T;
  payload: K;
}

export type ActivityDateEnd = number | 'NOT_SET';

export interface Channel {
  id: number;
  name: string;
}

export interface User {
  id: number;
  nickname: string;
  fullname: string;
  picture: string;
}

export interface ActivityLocation {
  city: string;
  street: string;
  latitude: string | number;
  longitude: string | number;
}

export interface ActivityComment {
  id: number;
  user: User;
  text: string;
  dateComment: number | string;
}

export interface Activity {
  id: number;
  author: User;
  channel: Channel;
  title: string;
  content: string;
  imageCollection: string[];
  thumbnail: string;
  datePosted: number;
  dateStart: number;
  dateEnd: ActivityDateEnd;
  userLike: User[];
  userGoing: User[];
  isGoing: boolean;
  isLike: boolean;
  location: ActivityLocation;
  commentCollection: ActivityComment[];
}

export interface ListById<T> {
  [id: string]: T;
}

export type FetchStatus = 'SUCCESS' | 'FAILURE' | 'PENDING';
