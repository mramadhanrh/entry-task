import React from 'react';
import { compose } from 'redux';
import './App.scss';

import withRedux from './components/hoc/withRedux';
import AppRoutes from './AppRoutes';

const App: React.FC = () => {
  return <AppRoutes />;
};

/**
 * HACK: using compose to make axiosInstance interceptors work
 * without this the redux thunk will return null at api parameter
 * when applied interceptors with redux stores at axiosInstance
 */
export default compose(withRedux)(App);
