import axios, { AxiosInstance, AxiosError } from 'axios';
import { stores } from 'src/redux';
import { setLoginStatus } from 'src/redux/auth/actions';

const apiVersion = 'v1';
const axiosInstance: AxiosInstance = axios.create({
  baseURL: `/api/${apiVersion}`,
  timeout: 10000,
});

axiosInstance.interceptors.request.use((config) => {
  config.headers = {
    ...config.headers,
    'user-token': stores.getState().auth.token,
  };

  return config;
});

axiosInstance.interceptors.response.use(
  (res) => res,
  (err: AxiosError) => {
    if (
      typeof err.response?.status === 'number' &&
      err.response?.status >= 403
    ) {
      stores.dispatch(setLoginStatus('LOGGED_OUT'));
    }
    return Promise.reject(err);
  }
);

export const routes = {
  login: '/login',
  channel: '/channel',
  activityFeed: `/activity/feed`,
  activityComment: (id: string, userId: number) =>
    `/activity/comment?id=${id}&user-id=${userId}`,
  activityDetail: (id: string): string => `/activity?id=${id}`,
  activityLike: (id: string, userId: number) =>
    `/activity/like?id=${id}&user-id=${userId}`,
  activityGoing: (id: string, userId: number) =>
    `/activity/going?id=${id}&user-id=${userId}`,
  activitySearch: (channel?: string, from?: number, to?: number) =>
    `/activity/search?channel=${channel || ''}&timestart=${
      from || ''
    }&timeend=${to || ''}`,
};

export default axiosInstance;
