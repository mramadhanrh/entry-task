export const SVGAssets = {
  CHECK_OUTLINE: '/static/media/svg/check-outline.svg',
  CHECK: '/static/media/svg/check.svg',
  COMMENT_SINGLE: '/static/media/svg/comment-single.svg',
  LIKE_OUTLINE: '/static/media/svg/like-outline.svg',
  LIKE: '/static/media/svg/like.svg',
  SEND: '/static/media/svg/send.svg',
  CROSS: '/static/media/svg/cross.svg',
  NO_ACTIVITY: '/static/media/svg/no-activity.svg',
  HOME: '/static/media/svg/home.svg',
  SEARCH: '/static/media/svg/search.svg',
  LOGO_CAT: '/static/media/svg/logo-cat.svg',
};

export const IMGAssets = {};
