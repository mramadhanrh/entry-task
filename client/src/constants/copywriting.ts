export default {
  appName: 'Black Cat',
  loginPage: {
    subtitle: 'Find The Most Loved Activities',
  },
  comment: {
    placeholder: 'Leave your comment here',
    newMessage: 'Scroll down to view new message!',
  },
};
