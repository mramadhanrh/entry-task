import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.scss';

import LoginPage from './pages/LoginPage';
import FeedPage from './pages/FeedPage';
import ActivityDetailPage from './pages/ActivityDetailPage';

const AppRoutes: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route path='/detail/:activityId'>
          <ActivityDetailPage />
        </Route>

        <Route path='/login'>
          <LoginPage />
        </Route>

        <Route path='/' exact={true}>
          <FeedPage />
        </Route>
      </Switch>
    </Router>
  );
};

export default AppRoutes;
