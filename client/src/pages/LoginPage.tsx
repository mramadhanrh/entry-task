import React, { FC, useEffect, useState, ChangeEvent } from 'react';

import FullContainerButton from '../components/atoms/FullContainerButton';
import LoginPageTemplate from '../templates/LoginPageTemplate';
import LoginForm from '../components/organisms/LoginForm';
import { requestLogin } from '../redux/auth/actions';
import { connect } from 'react-redux';
import { AppStateType } from '../redux/rootReducers';
import { useHistory } from 'react-router-dom';
import { LOGIN_STATUS_TYPE } from '../redux/auth/types';

interface LoginPageProps extends MapDispatch {
  loginStatus: LOGIN_STATUS_TYPE;
  onLogin(email: string, password: string): void;
}

const LoginPage: FC<LoginPageProps> = ({
  loginStatus = 'LOGGED_OUT',
  onLogin = () => void 0,
}) => {
  const history = useHistory();
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value },
    } = e;

    setEmail(value);
  };

  const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value },
    } = e;

    setPassword(value);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onLogin(email, password);
  };

  const footerButton = () => {
    return <FullContainerButton type='submit'>SIGN IN</FullContainerButton>;
  };

  useEffect(() => {
    if (loginStatus === 'LOGGED_IN') history.push('/');
  }, [loginStatus, history]);

  return (
    <form onSubmit={handleSubmit}>
      <LoginPageTemplate footer={footerButton()}>
        <LoginForm
          emailValue={email}
          passwordValue={password}
          onEmailChange={handleEmailChange}
          onPasswordChange={handlePasswordChange}
        />
      </LoginPageTemplate>
    </form>
  );
};

interface MapState {
  loginStatus: LOGIN_STATUS_TYPE;
}

const mapState = (state: AppStateType): MapState => {
  const { auth } = state;
  return {
    loginStatus: auth.loginStatus,
  };
};

interface MapDispatch {
  onLogin: (email: string, password: string) => void;
}

const mapDispatch = (dispatch): MapDispatch => {
  return {
    onLogin: (email: string, password: string) =>
      dispatch(requestLogin(email, password)),
  };
};

export default connect(mapState, mapDispatch)(LoginPage);
