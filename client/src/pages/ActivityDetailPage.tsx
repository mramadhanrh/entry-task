import React, { useEffect, FC, useState } from 'react';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';

import ActivityDetailPost from '../components/organisms/ActivityDetailPost';
import Menubar from '../components/organisms/Menubar';
import MenubarTemplate from '../templates/MenubarTemplate';

import { Activity, ListById, FetchStatus, User } from '../interfaces';

import {
  fetchActiveActivityData,
  postLikeActivity,
  postGoingActivity,
  postCommentActivity,
} from '../redux/activity/actions';
import { AppStateType } from '../redux/rootReducers';
import ActivityDetailFooter from '../components/organisms/ActivityDetailFooter';
import CommentFooter from '../components/organisms/CommentFooter';
import withSessionCheck from '../components/hoc/withSessionCheck';

const ActivityDetailPage: FC<MapDispatch & MapState> = ({
  data = {},
  user = {},
  onFetchDetail = () => void 0,
  onLikeActivity = () => void 0,
  onGoingActivity = () => void 0,
  onCommentActivity = () => void 0,
  status = {},
}) => {
  const { activityId } = useParams();
  const {
    channel,
    title,
    author,
    content,
    imageCollection,
    datePosted,
    dateStart,
    dateEnd,
    location,
    userGoing,
    userLike,
    isLike,
    isGoing,
    commentCollection,
  } = (data as ListById<Activity>)[activityId || ''] || {};
  const [commentAcive, setCommentActive] = useState<boolean>(false);
  const [mention, setMention] = useState<string>('');
  const [isToastActive, setIsToastActive] = useState<boolean>(false);

  const toggleComment = () => setCommentActive((state) => !state);

  const handleLikeClick = () => {
    if (activityId) onLikeActivity(activityId);
  };

  const handleJoinClick = () => {
    if (activityId) onGoingActivity(activityId);
  };

  const handleReplyClick = (replyUser: User) => {
    setMention(replyUser.nickname);
    setCommentActive(true);
  };

  const handleCommentSubmit = (comment: string) => {
    if (activityId && comment.trim() !== '') {
      onCommentActivity(activityId, comment);
      setMention('');
      setIsToastActive(true);
    }
  };

  useEffect(() => {
    if (activityId || activityId === '0') onFetchDetail(activityId);
  }, [activityId, onFetchDetail]);

  useEffect(() => {
    const handleToastHider = () => {
      const {
        scrollTop,
        scrollHeight,
        clientHeight,
      } = document.documentElement;

      if (scrollTop >= scrollHeight - clientHeight && isToastActive) {
        setIsToastActive(false);
      }
    };

    window.addEventListener('scroll', handleToastHider);

    return () => {
      window.removeEventListener('scroll', handleToastHider);
    };
  }, [isToastActive]);

  const header = () => {
    const { picture } = user as User;
    return <Menubar hideSearch={true} profilePic={picture} />;
  };
  const footer = (
    <>
      {commentAcive ? (
        <CommentFooter
          mention={mention}
          onSendComment={handleCommentSubmit}
          onClose={toggleComment}
        />
      ) : (
        <ActivityDetailFooter
          liked={isLike}
          joined={isGoing}
          onLikeClick={handleLikeClick}
          onJoinClick={handleJoinClick}
          onCommentClick={toggleComment}
        />
      )}
    </>
  );

  return (
    <MenubarTemplate header={header()} footer={footer}>
      {status[activityId || ''] === 'SUCCESS' && (
        <ActivityDetailPost
          location={location}
          channelName={channel?.name}
          dateStart={dateStart}
          dateEnd={dateEnd}
          datePosted={datePosted}
          title={title}
          author={author}
          content={content}
          images={imageCollection}
          userGoing={userGoing}
          userLike={userLike}
          liked={isLike}
          joined={isGoing}
          comments={commentCollection}
          toastActive={isToastActive}
          onReplyClick={handleReplyClick}
        />
      )}
    </MenubarTemplate>
  );
};

interface MapState {
  data: ListById<Activity>;
  status: ListById<FetchStatus>;
  user: object | User;
}

const mapState = (state: AppStateType): MapState => {
  const { activity, auth } = state;
  const { activeData: data, status } = activity;

  return {
    data,
    status,
    user: auth.user,
  };
};

interface MapDispatch {
  onFetchDetail: (id: string) => void;
  onLikeActivity: (id: string) => void;
  onGoingActivity: (id: string) => void;
  onCommentActivity: (id: string, comment: string) => void;
}

const mapDispatch = (dispatch): MapDispatch => {
  return {
    onFetchDetail: (id: string) => dispatch(fetchActiveActivityData(id)),
    onLikeActivity: (id: string) => dispatch(postLikeActivity(id)),
    onGoingActivity: (id: string) => dispatch(postGoingActivity(id)),
    onCommentActivity: (id: string, comment: string) => {
      dispatch(postCommentActivity(id, comment));
    },
  };
};

export default compose(
  withSessionCheck,
  connect(mapState, mapDispatch)
)(ActivityDetailPage);
