import React, { FC, useState, useEffect } from 'react';
import { connect } from 'react-redux';

import Menubar from '../components/organisms/Menubar';
import SearchSidebar from '../components/organisms/SearchSidebar';
import ActivityPostCardList from '../components/organisms/ActivityPostCardList';
import MenubarTemplate from '../templates/MenubarTemplate';

import { AppStateType } from '../redux/rootReducers';
import {
  fetchDataList,
  fetchSearchData,
  clearSearchData,
  postLikeFeedActivity,
  postGoingFeedActivity,
} from '../redux/activity/actions';

import { Activity, ListById, User } from '../interfaces';
import { compose } from 'redux';
import withSessionCheck from '../components/hoc/withSessionCheck';
import { fetchChannels } from '../redux/channels/actions';
import { DateCategoryResponse } from '../components/organisms/DateCategoryList/DateCategoryList';
import SearchResultCard from '../components/molecules/SearchResultCard';
import { getCopywritingDateRange } from '../helpers/dateHelper';

type FeedPageProps = MapState & MapDispatch;

const FeedPage: FC<FeedPageProps> = ({
  activityData = {},
  searchData = {},
  isSearch = false,
  channels = [],
  user = {},
  onLoadDataList = () => void 0,
  onActivityFeedLike = () => void 0,
  onActivityFeedGoing = () => void 0,
  onLoadChannel = () => void 0,
  onSearchActivity = () => void 0,
  onClearSearch = () => void 0,
}) => {
  const [siderActive, setSiderActive] = useState<boolean>(false);
  const [dateData, setDateData] = useState<DateCategoryResponse>();
  const [searchChannel, setSearchChannel] = useState<string>('');
  const data = isSearch ? searchData : activityData;
  const toggleSider = () => setSiderActive((state) => !state);

  const getDateRangeText = () => {
    if (dateData?.category === 'later') {
      return getCopywritingDateRange(dateData.from, dateData.to);
    }

    return `on ${dateData?.category}`;
  };

  const handleSearchClick = (e: DateCategoryResponse, channel: string) => {
    onSearchActivity(channel === 'All' ? '' : channel, e.from, e.to);
    setDateData(e);
    setSearchChannel(channel);
    toggleSider();
  };

  const header = () => {
    const { picture } = user as User;
    return <Menubar onSearchClick={toggleSider} profilePic={picture} />;
  };

  const sider = () => {
    return (
      <SearchSidebar channelData={channels} onSearchClick={handleSearchClick} />
    );
  };

  useEffect(() => {
    onLoadDataList(true);
  }, [onLoadDataList]);

  useEffect(() => {
    onLoadChannel();
  }, [onLoadChannel]);

  return (
    <MenubarTemplate
      header={header()}
      sider={sider()}
      siderActive={siderActive}
      onSiderCancel={toggleSider}
    >
      {isSearch && (
        <SearchResultCard
          channel={searchChannel}
          dateText={getDateRangeText()}
          resultCount={Object.values(searchData).length}
          onClearSearchClick={onClearSearch}
        />
      )}
      <ActivityPostCardList
        data={Object.values(data)}
        onUpdateData={onLoadDataList}
        onLikeClick={(id) => onActivityFeedLike(id.toString())}
        onGoingClick={(id) => onActivityFeedGoing(id.toString())}
      />
    </MenubarTemplate>
  );
};

interface MapState {
  activityData: ListById<Activity>;
  searchData: ListById<Activity>;
  isSearch: boolean;
  channels: string[];
  user: object | User;
}

const mapState = (state: AppStateType): MapState => {
  const { activity, channels, auth } = state;

  return {
    activityData: activity.data,
    searchData: activity.searchData,
    isSearch: activity.isSearch,
    channels: channels.data.map((item) => item.name),
    user: auth.user,
  };
};

interface MapDispatch {
  onLoadDataList: (initial?: boolean) => void;
  onActivityFeedLike: (id: string) => void;
  onActivityFeedGoing: (id: string) => void;
  onLoadChannel: () => void;
  onSearchActivity: (channel?: string, from?: number, to?: number) => void;
  onClearSearch: () => void;
}

const mapDispatch = (dispatch): MapDispatch => {
  return {
    onLoadDataList: (initial: boolean = false) =>
      dispatch(fetchDataList(initial)),
    onActivityFeedLike: (id: string) => dispatch(postLikeFeedActivity(id)),
    onActivityFeedGoing: (id: string) => dispatch(postGoingFeedActivity(id)),
    onLoadChannel: () => dispatch(fetchChannels()),
    onSearchActivity: (channel?: string, from?: number, to?: number) =>
      dispatch(fetchSearchData(channel, from, to)),
    onClearSearch: () => dispatch(clearSearchData()),
  };
};

export default compose(
  withSessionCheck,
  connect(mapState, mapDispatch)
)(FeedPage);
