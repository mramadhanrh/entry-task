import { User } from '../../interfaces';
import {
  AuthActionTypes,
  AuthLoginStatusAction,
  LOGIN_STATUS_TYPE,
  AUTH_LOGIN,
  AuthLoginAction,
  AUTH_LOGIN_STATUS,
  AuthSetToken,
  AUTH_SET_TOKEN,
} from './types';

interface AuthState {
  user: User | object;
  token?: string;
  loginStatus: LOGIN_STATUS_TYPE;
}

const initialState: AuthState = {
  user: {},
  token: undefined,
  loginStatus: 'LOGGED_OUT',
};

const auth = (
  state: AuthState = initialState,
  action: AuthActionTypes
): AuthState => {
  const { type } = action;

  const setUserData = (): AuthState => {
    return {
      ...state,
      user: (action as AuthLoginAction).payload,
    };
  };

  const setLoginStatus = (): AuthState => {
    return {
      ...state,
      loginStatus: (action as AuthLoginStatusAction).payload,
    };
  };

  const setAuthToken = (): AuthState => {
    return {
      ...state,
      token: (action as AuthSetToken).payload.token,
    };
  };

  switch (type) {
    case AUTH_LOGIN:
      return setUserData();
    case AUTH_LOGIN_STATUS:
      return setLoginStatus();
    case AUTH_SET_TOKEN:
      return setAuthToken();
    default:
      return state;
  }
};

export default auth;
