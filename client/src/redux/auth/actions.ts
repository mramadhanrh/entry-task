import { User } from '../../interfaces';
import {
  AuthActionTypes,
  AUTH_LOGIN,
  LOGIN_STATUS_TYPE,
  AUTH_LOGIN_STATUS,
  AUTH_SET_TOKEN,
} from './types';
import { AppStateType } from '../rootReducers';
import { AxiosInstance, AxiosResponse } from 'axios';
import { Dispatch } from 'redux';
import { routes } from '../../api/axiosInstance';

export const setUserAccount = (user: User): AuthActionTypes => {
  return {
    type: AUTH_LOGIN,
    payload: user,
  };
};

export const setLoginStatus = (status: LOGIN_STATUS_TYPE): AuthActionTypes => {
  return {
    type: AUTH_LOGIN_STATUS,
    payload: status,
  };
};

export const setUserToken = (token: string): AuthActionTypes => {
  return {
    type: AUTH_SET_TOKEN,
    payload: {
      token,
    },
  };
};

export const requestLogin = (email: string, password: string) => {
  return async (
    dispatch: Dispatch,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    try {
      dispatch(setLoginStatus('LOGGING_IN'));
      const { data }: AxiosResponse = await api.post(routes.login, {
        email,
        password,
      });
      const { data: userData, token }: { data: User; token: string } = data;
      dispatch(setUserAccount(userData));
      dispatch(setUserToken(token));
      dispatch(setLoginStatus('LOGGED_IN'));
    } catch (e) {
      dispatch(setLoginStatus('INVALID_ACCOUNT'));
    }
  };
};
