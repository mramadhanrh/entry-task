import { User } from '../../interfaces';

export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGIN_STATUS = 'AUTH_LOGIN_STATUS';
export const AUTH_SET_TOKEN = 'AUTH_SET_TOKEN';
export type LOGIN_STATUS_TYPE =
  | 'LOGGING_IN'
  | 'LOGGED_IN'
  | 'LOGGED_OUT'
  | 'INVALID_ACCOUNT';

export interface AuthLoginAction {
  type: typeof AUTH_LOGIN;
  payload: User;
}

export interface AuthLoginStatusAction {
  type: typeof AUTH_LOGIN_STATUS;
  payload: LOGIN_STATUS_TYPE;
}

export interface AuthSetToken {
  type: typeof AUTH_SET_TOKEN;
  payload: {
    token: string;
  };
}

export type AuthActionTypes =
  | AuthLoginAction
  | AuthLoginStatusAction
  | AuthSetToken;
