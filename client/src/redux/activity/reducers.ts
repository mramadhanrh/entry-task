import { Activity, ListById, FetchStatus, User } from '../../interfaces';
import {
  ACTIVITY_ADD_DATA,
  ACTIVITY_NEXT_FETCH_QUERY,
  ACTIVITY_ACTIVE_DATA,
  AddActivityListAction,
  ActivityActionTypes,
  ActivityActiveDataAction,
  ActivityActiveFetchStatusAction,
  FetchParamsAction,
  ACTIVITY_ACTIVE_DATA_FETCH_STATUS,
  LikeActivityAction,
  ACTIVITY_LIKE,
  GoingActivityAction,
  ACTIVITY_GOING,
  CommentActivityAction,
  ACTIVITY_COMMENT,
  LikeActivityFeedAction,
  GoingActivityFeedAction,
  ACTIVITY_FEED_LIKE,
  ACTIVITY_FEED_GOING,
  SetSearchActivityAction,
  ACTIVITY_SET_SEARCH_DATA,
  SetIsSearchActivityAction,
  ACTIVITY_SET_IS_SEARCH,
  ACTIVITY_CLEAR_SEARCH,
} from './types';

interface ActivityState {
  data: ListById<Activity>;
  searchData: ListById<Activity>;
  activeData: ListById<Activity>;
  status: ListById<FetchStatus>;
  isSearch: boolean;
  nextQuery: string | null;
}

const initialState: ActivityState = {
  data: {},
  searchData: {},
  activeData: {},
  status: {},
  isSearch: false,
  nextQuery: '',
};

const activity = (
  state: ActivityState = initialState,
  action: ActivityActionTypes
): ActivityState => {
  const { type } = action;

  const addData = (): ActivityState => {
    const { data } = (action as AddActivityListAction).payload;
    return {
      ...state,
      data: { ...state.data, ...data },
    };
  };

  const setActiveData = (): ActivityState => {
    const { activeData } = (action as ActivityActiveDataAction).payload;
    const { id } = activeData;

    return {
      ...state,
      activeData: {
        ...state.activeData,
        [id]: activeData,
      },
    };
  };

  const setNextFetchQuery = (): ActivityState => {
    const { nextQuery } = (action as FetchParamsAction).payload;
    return {
      ...state,
      nextQuery,
    };
  };

  const setActiveActivityFetchStatus = (): ActivityState => {
    const { id, status } = (action as ActivityActiveFetchStatusAction).payload;
    return {
      ...state,
      status: {
        ...state.status,
        [id]: status,
      },
    };
  };

  const likeActivity = (): ActivityState => {
    const { id, user } = (action as LikeActivityAction).payload;
    const { userLike, isLike } = state.activeData[id];

    return {
      ...state,
      activeData: {
        ...state.activeData,
        [id]: {
          ...state.activeData[id],
          isLike: !isLike,
          userLike: isLike
            ? userLike.filter((itemUser: User) => itemUser.id !== user.id)
            : [...userLike, user],
        },
      },
    };
  };

  const goingActivity = (): ActivityState => {
    const { id, user } = (action as GoingActivityAction).payload;
    const { userGoing, isGoing } = state.activeData[id];

    return {
      ...state,
      activeData: {
        ...state.activeData,
        [id]: {
          ...state.activeData[id],
          isGoing: !isGoing,
          userGoing: isGoing
            ? userGoing.filter((itemUser: User) => itemUser.id !== user.id)
            : [...userGoing, user],
        },
      },
    };
  };

  const commentActivity = (): ActivityState => {
    const { id, data } = (action as CommentActivityAction).payload;

    return {
      ...state,
      activeData: {
        ...state.activeData,
        [id]: {
          ...state.activeData[id],
          commentCollection: [
            ...state.activeData[id].commentCollection,
            { id: state.activeData[id].commentCollection.length, ...data },
          ],
        },
      },
    };
  };

  const likeFeedActivity = (): ActivityState => {
    const { id, user } = (action as LikeActivityFeedAction).payload;
    const { userLike, isLike } = state.data[id];

    return {
      ...state,
      data: {
        ...state.data,
        [id]: {
          ...state.data[id],
          isLike: !isLike,
          userLike: isLike
            ? userLike.filter((itemUser: User) => itemUser.id !== user.id)
            : [...userLike, user],
        },
      },
    };
  };

  const goingFeedActivity = (): ActivityState => {
    const { id, user } = (action as GoingActivityFeedAction).payload;
    const { userGoing, isGoing } = state.data[id];

    return {
      ...state,
      data: {
        ...state.data,
        [id]: {
          ...state.data[id],
          isGoing: !isGoing,
          userGoing: isGoing
            ? userGoing.filter((itemUser: User) => itemUser.id !== user.id)
            : [...userGoing, user],
        },
      },
    };
  };

  const setSearchData = (): ActivityState => {
    const { data } = (action as SetSearchActivityAction).payload;
    return {
      ...state,
      searchData: data,
    };
  };

  const setIsSearch = (): ActivityState => {
    const { isSearch } = (action as SetIsSearchActivityAction).payload;
    return {
      ...state,
      isSearch,
    };
  };

  const clearSearchData = (): ActivityState => {
    return {
      ...state,
      isSearch: false,
      searchData: {},
    };
  };

  switch (type) {
    case ACTIVITY_ADD_DATA:
      return addData();
    case ACTIVITY_NEXT_FETCH_QUERY:
      return setNextFetchQuery();
    case ACTIVITY_ACTIVE_DATA:
      return setActiveData();
    case ACTIVITY_ACTIVE_DATA_FETCH_STATUS:
      return setActiveActivityFetchStatus();
    case ACTIVITY_LIKE:
      return likeActivity();
    case ACTIVITY_GOING:
      return goingActivity();
    case ACTIVITY_COMMENT:
      return commentActivity();
    case ACTIVITY_FEED_LIKE:
      return likeFeedActivity();
    case ACTIVITY_FEED_GOING:
      return goingFeedActivity();
    case ACTIVITY_SET_SEARCH_DATA:
      return setSearchData();
    case ACTIVITY_SET_IS_SEARCH:
      return setIsSearch();
    case ACTIVITY_CLEAR_SEARCH:
      return clearSearchData();
    default:
      return state;
  }
};
export default activity;
