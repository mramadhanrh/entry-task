import { Dispatch } from 'redux';
import { AxiosInstance, AxiosResponse } from 'axios';
import {
  ActivityActionTypes,
  ACTIVITY_ADD_DATA,
  ACTIVITY_NEXT_FETCH_QUERY,
  ACTIVITY_ACTIVE_DATA,
  ACTIVITY_ACTIVE_DATA_FETCH_STATUS,
  ACTIVITY_LIKE,
  ACTIVITY_COMMENT,
  ACTIVITY_GOING,
  ACTIVITY_FEED_LIKE,
  ACTIVITY_FEED_GOING,
  ACTIVITY_SET_SEARCH_DATA,
  ACTIVITY_SET_IS_SEARCH,
  ACTIVITY_CLEAR_SEARCH,
} from './types';
import { Activity, FetchStatus, User, ListById } from '../../interfaces';
import { routes } from '../../api/axiosInstance';
import { AppStateType } from '../rootReducers';

export const addDataList = (
  newData: ListById<Activity>
): ActivityActionTypes => {
  return {
    type: ACTIVITY_ADD_DATA,
    payload: {
      data: newData,
    },
  };
};

export const setActivityNextFetchQuery = (
  nextQuery: string
): ActivityActionTypes => {
  return {
    type: ACTIVITY_NEXT_FETCH_QUERY,
    payload: {
      nextQuery,
    },
  };
};

export const setFetchActivityDetailStatus = (
  id: string,
  status: FetchStatus
): ActivityActionTypes => {
  return {
    type: ACTIVITY_ACTIVE_DATA_FETCH_STATUS,
    payload: {
      id,
      status,
    },
  };
};

export const fetchDataList = (initial: boolean = false) => {
  return async (
    dispatch: Dispatch,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    const {
      activity: { nextQuery },
    } = getState();
    const res: AxiosResponse = await api.get(
      `${routes.activityFeed}${initial ? '' : nextQuery || ''}`
    );

    const { data } = res;
    const { results = [], next = '' } = data;

    dispatch(setActivityNextFetchQuery(next));
    dispatch(addDataList(results));
  };
};

export const setActiveActivityData = (
  activeData: Activity
): ActivityActionTypes => ({
  type: ACTIVITY_ACTIVE_DATA,
  payload: {
    activeData,
  },
});

export const fetchActiveActivityData = (activityId: string) => {
  return async (
    dispatch: Dispatch,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    try {
      dispatch(setFetchActivityDetailStatus(activityId, 'PENDING'));

      const { data }: AxiosResponse = await api.get(
        routes.activityDetail(activityId)
      );
      const actDetailData: Activity = data;

      dispatch(setFetchActivityDetailStatus(activityId, 'SUCCESS'));
      dispatch(setActiveActivityData(actDetailData));
    } catch (e) {
      dispatch(setFetchActivityDetailStatus(activityId, 'FAILURE'));
    }
  };
};

export const likeActivity = (id: string, user: User): ActivityActionTypes => {
  return {
    type: ACTIVITY_LIKE,
    payload: {
      id,
      user,
    },
  };
};

export const postLikeActivity = (id: string) => {
  return async (
    dispatch: Dispatch | any,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    const { auth } = getState();
    const { user } = auth;

    await api.post(routes.activityLike(id, (user as User).id));

    dispatch(likeActivity(id, user as User));
  };
};

export const goingActivity = (id: string, user: User): ActivityActionTypes => {
  return {
    type: ACTIVITY_GOING,
    payload: {
      id,
      user,
    },
  };
};

export const postGoingActivity = (id: string) => {
  return async (
    dispatch: Dispatch,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    const { auth } = getState();
    const { user } = auth;

    await api.post(routes.activityGoing(id, (user as User).id));
    dispatch(goingActivity(id, user as User));
  };
};

export const commentActivity = (
  id: string,
  text: string,
  user: User
): ActivityActionTypes => {
  return {
    type: ACTIVITY_COMMENT,
    payload: {
      id,
      data: {
        text,
        user,
        dateComment: +new Date(),
      },
    },
  };
};

export const postCommentActivity = (id: string, text: string) => {
  return async (
    dispatch: Dispatch,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    const { auth } = getState();
    const { user } = auth;

    await api.post(routes.activityComment(id, (user as User).id), {
      text,
    });
    dispatch(commentActivity(id, text, user as User));
  };
};

export const likeFeedActivity = (
  id: string,
  user: User
): ActivityActionTypes => {
  return {
    type: ACTIVITY_FEED_LIKE,
    payload: {
      id,
      user,
    },
  };
};

export const postLikeFeedActivity = (id: string) => {
  return async (
    dispatch: Dispatch,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    const { auth } = getState();
    const { user } = auth;

    await api.post(routes.activityLike(id, (user as User).id));
    dispatch(likeFeedActivity(id, user as User));
  };
};

export const goingFeedActivity = (
  id: string,
  user: User
): ActivityActionTypes => {
  return {
    type: ACTIVITY_FEED_GOING,
    payload: {
      id,
      user,
    },
  };
};

export const postGoingFeedActivity = (id: string) => {
  return async (
    dispatch: Dispatch,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    const { auth } = getState();
    const { user } = auth;

    await api.post(routes.activityGoing(id, (user as User).id));
    dispatch(goingFeedActivity(id, user as User));
  };
};

export const setSearchData = (
  data: ListById<Activity>
): ActivityActionTypes => {
  return {
    type: ACTIVITY_SET_SEARCH_DATA,
    payload: {
      data,
    },
  };
};

export const setIsSearch = (isSearch: boolean): ActivityActionTypes => {
  return {
    type: ACTIVITY_SET_IS_SEARCH,
    payload: {
      isSearch,
    },
  };
};

export const fetchSearchData = (
  channel?: string,
  from?: number,
  to?: number
) => {
  return async (
    dispatch: Dispatch,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    const { data }: AxiosResponse = await api.get(
      routes.activitySearch(channel, from, to)
    );
    const { results } = data;
    dispatch(setIsSearch(true));
    dispatch(setSearchData(results));
  };
};

export const clearSearchData = (): ActivityActionTypes => {
  return {
    type: ACTIVITY_CLEAR_SEARCH,
  };
};
