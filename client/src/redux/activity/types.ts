import {
  Activity,
  FetchStatus,
  ActivityComment,
  ListById,
  User,
} from '../../interfaces';

export const ACTIVITY_ADD_DATA = 'ACTIVITY_ADD_DATA';
export const ACTIVITY_ACTIVE_DATA = 'ACTIVITY_ACTIVE_DATA';
export const ACTIVITY_ACTIVE_DATA_FETCH_STATUS =
  'ACTIVITY_ACTIVE_DATA_FETCH_STATUS';
export const ACTIVITY_NEXT_FETCH_QUERY = 'ACTIVITY_NEXT_FETCH_QUERY';

export const ACTIVITY_FEED_LIKE = 'ACTIVITY_FEED_LIKE';
export const ACTIVITY_LIKE = 'ACTIVITY_LIKE';
export const ACTIVITY_FEED_GOING = 'ACTIVITY_FEED_GOING';
export const ACTIVITY_GOING = 'ACTIVITY_GOING';
export const ACTIVITY_COMMENT = 'ACTIVITY_COMMENT';
export const ACTIVITY_SET_SEARCH_DATA = 'ACTIVITY_SET_SEARCH_DATA';
export const ACTIVITY_SET_IS_SEARCH = 'ACTIVITY_SET_IS_SEARCH';
export const ACTIVITY_CLEAR_SEARCH = 'ACTIVITY_CLEAR_SEARCH';

export interface AddActivityListAction {
  type: typeof ACTIVITY_ADD_DATA;
  payload: {
    data: ListById<Activity>;
  };
}

export interface FetchParamsAction {
  type: typeof ACTIVITY_NEXT_FETCH_QUERY;
  payload: {
    nextQuery: string;
  };
}

export interface ActivityActiveDataAction {
  type: typeof ACTIVITY_ACTIVE_DATA;
  payload: {
    activeData: Activity;
  };
}

export interface ActivityActiveFetchStatusAction {
  type: typeof ACTIVITY_ACTIVE_DATA_FETCH_STATUS;
  payload: {
    id: string;
    status: FetchStatus;
  };
}

export interface LikeActivityFeedAction {
  type: typeof ACTIVITY_FEED_LIKE;
  payload: {
    id: string;
    user: User;
  };
}

export interface LikeActivityAction {
  type: typeof ACTIVITY_LIKE;
  payload: {
    id: string;
    user: User;
  };
}

export interface GoingActivityFeedAction {
  type: typeof ACTIVITY_FEED_GOING;
  payload: {
    id: string;
    user: User;
  };
}

export interface GoingActivityAction {
  type: typeof ACTIVITY_GOING;
  payload: {
    id: string;
    user: User;
  };
}

export interface CommentActivityAction {
  type: typeof ACTIVITY_COMMENT;
  payload: {
    id: string;
    data: Omit<ActivityComment, 'id'>;
  };
}

export interface SetSearchActivityAction {
  type: typeof ACTIVITY_SET_SEARCH_DATA;
  payload: {
    data: ListById<Activity>;
  };
}

export interface SetIsSearchActivityAction {
  type: typeof ACTIVITY_SET_IS_SEARCH;
  payload: {
    isSearch: boolean;
  };
}

export interface ClearSearchAction {
  type: typeof ACTIVITY_CLEAR_SEARCH;
}

export type ActivityActionTypes =
  | AddActivityListAction
  | FetchParamsAction
  | ActivityActiveDataAction
  | ActivityActiveFetchStatusAction
  | LikeActivityFeedAction
  | LikeActivityAction
  | GoingActivityFeedAction
  | GoingActivityAction
  | CommentActivityAction
  | SetSearchActivityAction
  | SetIsSearchActivityAction
  | ClearSearchAction;
