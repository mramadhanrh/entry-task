import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { rootReducers } from './rootReducers';
import axiosInstance from '../api/axiosInstance';

const persistConfig = {
  storage,
  key: 'root',
  whitelist: ['auth'],
};

const composeEnhancers = composeWithDevTools({
  name: 'Black Cat',
});

const persistedReducer = persistReducer(persistConfig, rootReducers);

export const stores = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(thunk.withExtraArgument(axiosInstance)))
);

export const persistor = persistStore(stores);
