import { AxiosResponse, AxiosInstance } from 'axios';
import { Dispatch } from 'redux';
import { ChannelActionTypes, CHANNEL_ADD } from './types';
import { AppStateType } from '../rootReducers';
import { routes } from '../../api/axiosInstance';
import { Channel } from '../../interfaces';

export const addChannels = (data): ChannelActionTypes => {
  return {
    type: CHANNEL_ADD,
    payload: {
      data,
    },
  };
};

export const fetchChannels = () => {
  return async (
    dispatch: Dispatch,
    getState: () => AppStateType,
    api: AxiosInstance
  ) => {
    const { data }: AxiosResponse = await api.get(routes.channel);
    const { channels }: { channels: Channel[] } = data;
    dispatch(addChannels(channels));
  };
};
