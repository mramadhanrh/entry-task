import { Channel } from '../../interfaces';
import { ChannelActionTypes, CHANNEL_ADD, ChannelAddAction } from './types';

interface ChannelState {
  data: Channel[];
}

const initialState: ChannelState = {
  data: [],
};

const channels = (
  state: ChannelState = initialState,
  action: ChannelActionTypes
): ChannelState => {
  const { type } = action;

  if (type === CHANNEL_ADD) {
    return { ...state, data: (action as ChannelAddAction).payload.data };
  }

  return state;
};

export default channels;
