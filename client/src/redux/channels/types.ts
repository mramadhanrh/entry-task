import { Channel } from '../../interfaces';

export const CHANNEL_ADD = 'CHANNEL_ADD';

export interface ChannelAddAction {
  type: typeof CHANNEL_ADD;
  payload: {
    data: Channel[];
  };
}

export type ChannelActionTypes = ChannelAddAction;
