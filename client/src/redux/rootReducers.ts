import { combineReducers } from 'redux';
import activity from './activity/reducers';
import auth from './auth/reducers';
import channels from './channels/reducers';

export const rootReducers = combineReducers({
  activity,
  auth,
  channels,
});

export type AppStateType = ReturnType<typeof rootReducers>;
