import React, { FC, memo } from 'react';
import styles from './index.module.scss';

const FullContainerButton: FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = (
  props
) => {
  return (
    <>
      {/* eslint-disable-next-line react/button-has-type */}
      <button {...props} className={`${styles.button} ${props.className}`} />
    </>
  );
};

export default memo(FullContainerButton);
