import React, { FC, memo } from 'react';
import styles from './index.module.scss';

interface ChannelBadgeProps {
  name?: string;
}

const ChannelBadge: FC<ChannelBadgeProps> = ({ name = '' }) => {
  if (!name) return <div />;
  return (
    <div className={styles.container}>
      <p>{name}</p>
    </div>
  );
};

export default memo(ChannelBadge);
