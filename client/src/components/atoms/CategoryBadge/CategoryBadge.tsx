import React, { FC, memo } from 'react';
import styles from './index.module.scss';

interface CategoryBadgeProps {
  className?: string;
  activeClassname?: string;
  text?: string;
  active?: boolean;
  onClick?(): void;
  fontWeight?: 'normal' | 'semibold' | 'bold';
  borderless?: boolean;
  uppercase?: boolean;
}

const CategoryBadge: FC<CategoryBadgeProps> = ({
  className = '',
  activeClassname = '',
  text = '',
  active = false,
  onClick = () => void 0,
  fontWeight = 'normal',
  borderless = false,
  uppercase = false,
}) => {
  const textStyle = {
    border: borderless ? '' : styles['text--border'],
    weight: styles[`text--${fontWeight}`],
    active: active ? `${styles['text--active']} ${activeClassname}` : '',
    uppercase: uppercase ? styles['text--uppercase'] : '',
  };

  const getTextStyle = () => Object.values(textStyle).join(' ');

  return (
    <button
      type='button'
      className={`${styles.button} ${className}`}
      onClick={onClick}
    >
      <p className={`${styles.text} ${getTextStyle()}`}>{text}</p>
    </button>
  );
};

export default memo(CategoryBadge);
