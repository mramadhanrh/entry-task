import React, { FC, memo } from 'react';
import styles from './index.module.scss';

type DatePickerProps = Omit<
  React.InputHTMLAttributes<HTMLInputElement>,
  'type'
>;

const DatePicker: FC<DatePickerProps> = (props) => {
  return (
    <input
      {...props}
      type='date'
      className={`${styles.datepicker} ${props.className}`}
    />
  );
};

export default memo(DatePicker);
