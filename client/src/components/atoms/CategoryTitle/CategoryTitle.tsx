import React, { FC, memo } from 'react';
import styles from './index.module.scss';

interface CategoryTitleProps {
  text?: string;
}

const CategoryTitle: FC<
  CategoryTitleProps & React.HTMLAttributes<HTMLParagraphElement>
> = ({ text = '', ...props }) => {
  return (
    <p {...props} className={`${styles.title} ${props.className}`}>
      {text}
    </p>
  );
};

export default memo(CategoryTitle);
