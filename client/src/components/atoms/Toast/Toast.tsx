import React, { FC } from 'react';
import styles from './index.module.scss';

interface ToastProps {
  className?: string;
  text?: string;
  active?: boolean;
}

const Toast: FC<ToastProps> = ({
  className = '',
  text = '',
  active = false,
}) => {
  return (
    <div className={`${styles.container} ${className}`}>
      <div
        className={`${styles.toast} ${active ? styles['toast--active'] : ''}`}
      >
        {text}
      </div>
    </div>
  );
};

export default Toast;
