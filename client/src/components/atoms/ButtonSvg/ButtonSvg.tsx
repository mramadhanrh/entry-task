import React, { FC, memo } from 'react';
import SVG from 'react-inlinesvg';
import styles from './index.module.scss';

interface ButtonSvgProps {
  onClick?(): void;
  className?: string;
  type?: 'submit' | 'reset' | 'button';
}

type SVGProps = React.ComponentProps<typeof SVG>;

const ButtonSvg: FC<SVGProps & ButtonSvgProps> = ({
  onClick = () => void 0,
  className = '',
  type = 'button',
  ...props
}) => {
  return (
    <>
      {/* eslint-disable-next-line react/button-has-type */}
      <button
        type={type}
        className={`${styles.button} ${className}`}
        onClick={onClick}
      >
        <SVG {...props} />
      </button>
    </>
  );
};

export default memo(ButtonSvg);
