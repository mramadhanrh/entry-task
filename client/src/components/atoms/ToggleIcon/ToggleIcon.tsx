import React, { memo, FC } from 'react';
import SVG from 'react-inlinesvg';
import styles from './index.module.scss';

interface ToggleIconProps {
  className?: string;
  activeClassname?: string;
  icon: string;
  activeIcon: string;
  active?: boolean;
  color?: string;
  activeColor?: string;
}

const ToggleIcon: FC<ToggleIconProps> = ({
  className = '',
  activeClassname = '',
  icon = '',
  activeIcon = '',
  active = false,
  color = '',
  activeColor = '',
}) => {
  return (
    <>
      <span
        className={`${styles.toggle_icon} ${className} ${
          active ? `${styles['toggle_icon--active']} ${activeClassname}` : ''
        }`}
        style={{ fill: active ? activeColor : color }}
      >
        <SVG
          src={active ? activeIcon : icon}
          preProcessor={(code) =>
            code.replace(/fill=".*?"/g, 'fill="currentColor"')
          }
        />
      </span>
    </>
  );
};

export default memo(ToggleIcon);
