import React, { FC, memo } from 'react';
import SVG from 'react-inlinesvg';

import styles from './index.module.scss';

interface InputFieldIcon {
  icon?: string;
  src?: string;
  alt?: string;
}

const InputFieldIcon: FC<
  InputFieldIcon & React.InputHTMLAttributes<HTMLInputElement>
> = ({ icon = '', src = '', alt = '', ...props }) => {
  return (
    <span className={styles.container}>
      {src && <SVG alt={alt} src={src} />}
      <input {...props} />
    </span>
  );
};
export default memo(InputFieldIcon);
