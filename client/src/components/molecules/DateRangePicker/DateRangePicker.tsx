import React, { FC, memo } from 'react';
import SVG from 'react-inlinesvg';
import styles from './index.module.scss';

import DatePicker from '../../atoms/DatePicker';

interface DateRangePickerProps {
  valueFrom?: string;
  valueTo?: string;
  onToChange?(event: React.ChangeEvent<HTMLInputElement>): void;
  onFromChange?(event: React.ChangeEvent<HTMLInputElement>): void;
}

const DateRangePicker: FC<DateRangePickerProps> = ({
  valueFrom = '',
  valueTo = '',
  onToChange = () => void 0,
  onFromChange = () => void 0,
}) => {
  return (
    <div className={styles.container}>
      <div id='date-from'>
        <span>
          <SVG src='/static/media/svg/date-from.svg' />
        </span>
        <DatePicker onChange={onFromChange} value={valueFrom} />
      </div>
      <span className={styles.dash} />
      <div id='date-to'>
        <span>
          <SVG src='/static/media/svg/date-to.svg' />
        </span>
        <DatePicker onChange={onToChange} value={valueTo} />
      </div>
    </div>
  );
};

export default memo(DateRangePicker);
