import React, { useState, useEffect, useRef, FC, memo } from 'react';
import styles from './index.module.scss';

interface CarouselProps {
  className?: string;
  images: string[];
  duration?: number;
}

const Carousel: FC<CarouselProps> = ({
  className = '',
  images = [],
  duration = 5000,
}) => {
  const [activeIndex, setActiveIndex] = useState<number>(0);
  const carouselEl = useRef<HTMLDivElement>(null);
  const imgsEl = useRef<HTMLDivElement[]>([]);
  const intervalRef = useRef<NodeJS.Timeout | null>(null);

  useEffect(() => {
    if (carouselEl.current && imgsEl.current[activeIndex]) {
      carouselEl.current.scrollTo(imgsEl.current[activeIndex].offsetLeft, 0);
    }
  }, [activeIndex]);

  useEffect(() => {
    if (images.length > 0 && intervalRef.current === null) {
      intervalRef.current = setInterval(() => {
        setActiveIndex((state) => (state < images.length - 1 ? state + 1 : 0));
      }, duration);
    }

    return () => {
      if (intervalRef.current) clearInterval(intervalRef.current);
    };
  }, [images, duration]);

  return (
    <div className={`${styles.carousel} ${className}`} ref={carouselEl}>
      {images.map((url, idx) => {
        return (
          <div
            key={idx}
            ref={(ref) => (ref ? (imgsEl.current[idx] = ref) : null)}
            className={styles.carousel_img_wrapper}
          >
            <div
              className={styles.carousel__img}
              style={{
                backgroundImage: `url(${url})`,
              }}
            />
          </div>
        );
      })}
    </div>
  );
};

export default memo(Carousel);
