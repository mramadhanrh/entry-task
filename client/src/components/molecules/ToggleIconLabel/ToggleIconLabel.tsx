import React, { FC, memo } from 'react';
import styles from './index.module.scss';
import IconLabel from '../IconLabel';

interface ToggleIconLabelProps {
  className?: string;
  activeText?: string;
  activeTextColor?: string;
  activeIcon?: string | React.ReactNode;
  activeIconColor?: string;
  active?: boolean;
}

const ToggleIconLabel: FC<
  ToggleIconLabelProps & React.ComponentProps<typeof IconLabel>
> = ({
  className = '',
  active = false,
  activeText = '',
  activeTextColor = '#453257',
  activeIcon = null,
  activeIconColor = '#453257',
  onClick = () => void 0,
  ...props
}) => {
  const text = active ? activeText : props.text;
  const textColor = active ? activeTextColor : props.textColor;
  const icon = active ? activeIcon : props.icon;
  const iconColor = active ? activeIconColor : props.iconColor;

  return (
    <IconLabel
      className={`${active ? styles.active : ''} ${className}`}
      text={text}
      textColor={textColor}
      icon={icon}
      iconColor={iconColor}
      onClick={onClick}
    />
  );
};

export default memo(ToggleIconLabel);
