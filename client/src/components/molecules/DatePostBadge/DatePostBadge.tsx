import React, { useMemo, FC, memo } from 'react';
import SVG from 'react-inlinesvg';

import styles from './index.module.scss';

import { getReadableDateFormat } from '../../../helpers/dateHelper';
import { ActivityDateEnd } from '../../../interfaces';

interface DatePostBadgeProps {
  icon?: string;
  timestart?: number | string;
  timeend?: ActivityDateEnd;
}

const DatePostBadge: FC<DatePostBadgeProps> = ({
  icon = '/static/media/svg/time.svg',
  timestart = 'Unknown',
  timeend = 'NOT_SET' as ActivityDateEnd,
}) => {
  const startDate = useMemo(
    () =>
      typeof timestart === 'string'
        ? timestart
        : getReadableDateFormat(new Date(timestart)),
    [timestart]
  );
  const endDate = useMemo(
    () =>
      timeend === 'NOT_SET'
        ? 'Until Finish'
        : getReadableDateFormat(new Date(timeend)),
    [timeend]
  );

  return (
    <div className={styles.container}>
      <span>
        <SVG src={icon} />
      </span>
      <p>
        {startDate} - {endDate}
      </p>
    </div>
  );
};

export default memo(DatePostBadge);
