import React, { FC, memo } from 'react';
import SVG from 'react-inlinesvg';

import styles from './index.module.scss';
import { ActivityComment, User } from '../../../interfaces';
import { getPastDateRange } from '../../../helpers/dateHelper';

interface CommentItemProps extends Omit<ActivityComment, 'id'> {
  onReplyClick?(user: User): void;
}

const CommentItem: FC<CommentItemProps> = ({
  text = '',
  dateComment = '',
  user = {
    id: 0,
    fullname: '',
    nickname: '',
    picture: '',
  },
  onReplyClick = () => void 0,
}) => {
  const handleReplyClick = () => onReplyClick(user);

  return (
    <div className={styles.container}>
      <div
        className={styles.profile_pic}
        style={{ backgroundImage: `url(${user.picture})` }}
      />
      <div className={styles.comment_wrapper}>
        <div className={styles.user_info_wrapper}>
          <div className={styles.user_info}>
            <p className={styles.user_name}>{user.fullname}</p>
            <p className={styles.comment_date}>
              {getPastDateRange(new Date(dateComment))}
            </p>
          </div>
          <button className={styles.reply_btn} onClick={handleReplyClick}>
            <span className={styles.reply_icon}>
              <SVG src={'/static/media/svg/reply.svg'} />
            </span>
          </button>
        </div>
        <p className={styles.comment_text}>{text}</p>
      </div>
    </div>
  );
};

export default memo(CommentItem);
