import React, { FC, memo } from 'react';
import styles from './index.module.scss';

interface SearchResultCardProps {
  channel?: string;
  dateText?: string;
  resultCount?: number;
  onClearSearchClick?(): void;
}

const SearchResultCard: FC<SearchResultCardProps> = ({
  channel = '',
  dateText = '',
  resultCount = 0,
  onClearSearchClick = () => void 0,
}) => {
  return (
    <div className={styles.container}>
      <div className={styles.result_block}>
        <p className={styles.result_count}>{`${resultCount} Results`}</p>
        <button className={styles.clear_btn} onClick={onClearSearchClick}>
          Clear Search
        </button>
      </div>
      <p
        className={styles.search_info}
      >{`Searched for ${channel} ${dateText}`}</p>
    </div>
  );
};

export default memo(SearchResultCard);
