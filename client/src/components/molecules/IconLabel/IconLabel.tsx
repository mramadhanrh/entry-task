import React, { FC, memo } from 'react';
import SVG from 'react-inlinesvg';

import styles from './index.module.scss';

interface IconLabelProps {
  icon?: string | React.ReactNode;
  text?: string;
  className?: string;
  iconColor?: string;
  textColor?: string;
  onClick?(): void;
}

const IconLabel: FC<IconLabelProps> = ({
  icon = null,
  text = '',
  className = '',
  iconColor = '',
  textColor = '',
  onClick = () => void 0,
}) => {
  return (
    <button
      type='button'
      className={`${styles.container} ${className}`}
      onClick={onClick}
    >
      <span style={{ fill: iconColor }}>
        {typeof icon === 'string' ? (
          <SVG
            src={icon}
            preProcessor={(code) =>
              code.replace(/fill=".*?"/g, 'fill="currentColor"')
            }
          />
        ) : (
          icon
        )}
      </span>
      <p style={{ color: textColor }}>{text}</p>
    </button>
  );
};

export default memo(IconLabel);
