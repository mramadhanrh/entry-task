import React, { FC, memo } from 'react';
import styles from './index.module.scss';

interface AvatarUsernameProps {
  name?: string;
  src?: string;
}

const AvatarUsername: FC<AvatarUsernameProps> = ({ name = '', src = '' }) => {
  return (
    <div className={styles.container}>
      <img alt='profile-pic' src={src || '/static/media/svg/user.svg'} />
      <p>{name}</p>
    </div>
  );
};

export default memo(AvatarUsername);
