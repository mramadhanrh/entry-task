import React, { FC, useEffect } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { AppStateType } from '../../../redux/rootReducers';
import { LOGIN_STATUS_TYPE } from '../../../redux/auth/types';

export default (WrappedComponent) => {
  const HOCComponent: FC<MapState> = ({ loginStatus, ...props }) => {
    const history = useHistory();

    useEffect(() => {
      if (loginStatus !== 'LOGGED_IN') history.push('/login');
    }, [loginStatus, history]);

    return <WrappedComponent {...props} />;
  };

  interface MapState {
    loginStatus: LOGIN_STATUS_TYPE;
  }

  const mapState = (state: AppStateType): MapState => {
    const { auth } = state;

    return {
      loginStatus: auth.loginStatus,
    };
  };

  return connect(mapState)(HOCComponent);
};
