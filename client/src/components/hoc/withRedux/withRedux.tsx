import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { stores, persistor } from '../../../redux';

export default <P extends object>(WrappedComponent: React.ComponentType<P>) => (
  props: P
) => {
  return (
    <Provider store={stores}>
      <PersistGate loading={null} persistor={persistor}>
        <WrappedComponent {...props} />
      </PersistGate>
    </Provider>
  );
};
