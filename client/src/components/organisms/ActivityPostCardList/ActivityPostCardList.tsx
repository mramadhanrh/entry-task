import React, { useEffect, useRef, FC } from 'react';
import { debounce } from 'lodash';
import styles from './index.module.scss';

import ActivityPostCard from '../ActivityPostCard/ActivityPostCard';
import { Activity } from '../../../interfaces';
import NoActivityData from './NoActivityData';

interface ActivityPostCardListProps {
  data: Activity[];
  onUpdateData?(): void;
  onLikeClick?(id: number): void;
  onGoingClick?(id: number): void;
}

const ActivityPostCardList: FC<ActivityPostCardListProps> = ({
  data = [],
  onUpdateData = () => void 0,
  onLikeClick = () => void 0,
  onGoingClick = () => void 0,
}) => {
  const containerRef = useRef<HTMLDivElement>(null);

  const handleScroll = debounce(() => {
    if (
      window.innerHeight + document.documentElement.scrollTop >=
      document.documentElement.offsetHeight - 700
    ) {
      onUpdateData();
    }
  }, 200);

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [handleScroll]);

  if (data.length === 0) return <NoActivityData />;

  return (
    <div
      onScroll={handleScroll}
      className={styles.container}
      ref={containerRef}
    >
      {data.map((item) => {
        const {
          id,
          title,
          content,
          thumbnail,
          dateStart,
          dateEnd,
          userLike,
          userGoing,
          isLike,
          isGoing,
          author,
        } = item;

        return (
          <ActivityPostCard
            key={id}
            href={`/detail/${id}`}
            author={author}
            title={title}
            description={content}
            dateStart={dateStart}
            dateEnd={dateEnd}
            postImg={thumbnail}
            likeCount={userLike.length}
            goingCount={userGoing.length}
            liked={isLike}
            going={isGoing}
            onLikeClick={() => onLikeClick(id)}
            onGoingClick={() => onGoingClick(id)}
          />
        );
      })}
    </div>
  );
};

export default ActivityPostCardList;
