import React, { FC, memo } from 'react';
import SVG from 'react-inlinesvg';
import styles from './index.module.scss';
import { SVGAssets } from '../../../constants/assetsPath';

const NoActivityData: FC<{}> = () => {
  return (
    <div className={styles.no_activity_container}>
      <span className={styles.icon}>
        <SVG src={SVGAssets.NO_ACTIVITY} />
      </span>
      <p className={styles.text}>No Activity</p>
    </div>
  );
};

export default memo(NoActivityData);
