import React, { useState, FC, useRef, memo } from 'react';
import SVG from 'react-inlinesvg';

import styles from './index.module.scss';
import { User } from '../../../interfaces';
import ToggleIconLabel from '../../molecules/ToggleIconLabel';

interface ActivityPeopleListProps {
  users?: User[];
  icon: string;
  activeIcon: string;
  activeIconColor: string;
  active?: boolean;
  text: string;
  borderBottom?: boolean;
}

const DEFAULT_IMG_HEIGHT = 24;

const ActivityPeopleList: FC<ActivityPeopleListProps> = ({
  users = [],
  icon = '',
  activeIcon = '',
  activeIconColor = '#67616D',
  active = false,
  text = '',
  borderBottom = false,
}) => {
  const [expanded, setExpand] = useState(false);
  const peopleListEl = useRef<HTMLDivElement>(null);

  const toggleExpand = () => setExpand((state) => !state);

  const getUserList = () => {
    const max = 6;
    return users?.map((user, idx) => {
      const { id, picture } = user || {};

      return (
        <React.Fragment key={id}>
          <div
            style={{
              backgroundImage: `url(${picture})`,
            }}
            className={
              idx >= max && !expanded ? styles['people_img--hide'] : ''
            }
          />
        </React.Fragment>
      );
    });
  };

  const getPeopleListHeight = () => {
    if (peopleListEl.current) return peopleListEl.current.scrollHeight;

    return DEFAULT_IMG_HEIGHT;
  };

  return (
    <div className={styles.people_container}>
      <div className={borderBottom ? styles.people_separator : ''}>
        <ToggleIconLabel
          className={styles.interaction_button}
          icon={icon}
          activeIcon={activeIcon}
          activeIconColor={activeIconColor}
          text={text}
          activeText={text}
          active={active}
        />
        <div
          ref={peopleListEl}
          className={`${styles.people_img} ${
            expanded ? styles['people_img--active'] : ''
          }`}
          style={{
            maxHeight: expanded ? getPeopleListHeight() : DEFAULT_IMG_HEIGHT,
          }}
        >
          {getUserList()}
        </div>
        <button
          key='btn-expand'
          type='button'
          className={`${styles.people_btn_expand} ${
            expanded ? styles['people_btn_expand--hide'] : ''
          }`}
          onClick={toggleExpand}
        >
          <SVG src='/static/media/svg/chevron-down.svg' />
        </button>
      </div>
    </div>
  );
};

export default memo(ActivityPeopleList);
