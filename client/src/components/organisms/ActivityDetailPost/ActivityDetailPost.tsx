import React, { useRef, useState, FC, useEffect, memo } from 'react';
import ReactMarkdown from 'react-markdown';
import styles from './index.module.scss';

import ChannelBadge from '../../atoms/ChannelBadge';
import {
  User,
  ActivityDateEnd,
  ActivityLocation,
  ActivityComment,
} from '../../../interfaces';
import ActivityDetailTabNav from '../ActivityDetailTabNav';
import Carousel from '../../molecules/Carousel';
import ActivityLocationPost from './ActivityLocationPost';
import ActivityTimeBlock from './ActivityTimeBlock';
import { getPastDateRange } from '../../../helpers/dateHelper';
import ActivityPeopleList from './ActivityPeopleList';
import ActivityCommentList from './ActivityCommentList';
import {
  ActivityTabCategory,
  ActivityTabCategoryMap,
} from '../ActivityDetailTabNav/ActivityDetailTabNav';
import {
  getElementPosition,
  isOverElement,
} from '../../../helpers/elementHelper';

interface ActivityDetailPostProps {
  channelName?: string;
  title?: string;
  author?: User;
  datePosted?: number | null;
  dateStart?: number | string;
  dateEnd?: ActivityDateEnd;
  content?: string;
  images?: string[];
  initialHeight?: number;
  location?: ActivityLocation;
  userGoing?: User[];
  userLike?: User[];
  liked?: boolean;
  joined?: boolean;
  comments?: ActivityComment[];
  toastActive?: boolean;
  onReplyClick?(user: User): void;
}

const HEADER_HEIGHT = 86;

const ActivityDetailPost: FC<ActivityDetailPostProps> = ({
  channelName = '',
  title = '',
  author = { id: 0, nickname: '', fullname: '', picture: '' },
  datePosted = null,
  dateStart = 'Unknown',
  dateEnd = 'NOT_SET',
  content = '',
  images = [],
  initialHeight = 300,
  location = { city: '', latitude: 0, longitude: 0, street: '' },
  userGoing = [],
  userLike = [],
  liked = false,
  joined = false,
  comments = [],
  toastActive = false,
  onReplyClick = () => void 0,
}) => {
  const contentEl = useRef<HTMLElement>(null);
  const peopleEl = useRef<HTMLDivElement>(null);
  const commentEl = useRef<HTMLDivElement>(null);
  const elMap: ActivityTabCategoryMap<any> = {
    Details: contentEl.current,
    Comments: commentEl.current,
    Participants: peopleEl.current,
  };
  const [contentActive, setContentActive] = useState<boolean>(false);
  const [activeSection, setActiveSection] = useState<ActivityTabCategory>(
    'Details'
  );

  const toggleContentActive = () => setContentActive((state) => !state);

  const handleViewAllClick = () => {
    if (contentEl.current) toggleContentActive();
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleScrollEvent = () => {
    if (!commentEl.current && !peopleEl.current) return;
    const { scrollTop = 0 } = document.documentElement;

    if (isOverElement(commentEl.current, scrollTop + HEADER_HEIGHT)) {
      setActiveSection('Comments');
      return;
    }
    if (isOverElement(peopleEl.current, scrollTop + HEADER_HEIGHT)) {
      setActiveSection('Participants');
      return;
    }

    setActiveSection('Details');
  };

  const scrollToElement = (x: number, y: number) => {
    window.scrollTo(x, y - HEADER_HEIGHT);
  };

  const handleSelect = (value: ActivityTabCategory) => {
    if (value === 'Details') {
      scrollToElement(0, 0);
      return;
    }

    if (elMap[value]) {
      const { x, y } = getElementPosition(elMap[value]);
      if (x !== undefined && y !== undefined) {
        scrollToElement(x, y);
      }
    }
  };

  const getContentHeight = (): number | 'max-height' => {
    if (!contentActive) return initialHeight;

    return contentEl.current ? contentEl.current.scrollHeight : 'max-height';
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScrollEvent);

    return () => {
      window.removeEventListener('scroll', handleScrollEvent);
    };
  }, [handleScrollEvent]);

  return (
    <div className={styles.container}>
      <section about='header' className={styles.header}>
        <ChannelBadge name={channelName} />

        <section about='post-header-info' className={styles.header_info}>
          <h3 className={styles.title}>{title}</h3>

          <div id='author' className={styles.author}>
            <div
              className={styles.author_img}
              style={{ backgroundImage: `url(${author?.picture})` }}
            />
            <div className={styles.author_info}>
              <p className={styles.author_name}>{author?.fullname}</p>
              <p className={styles.author_publish}>
                {`Published ${
                  datePosted
                    ? getPastDateRange(new Date(datePosted))
                    : 'Unknown'
                }`}
              </p>
            </div>
          </div>
        </section>
      </section>

      <ActivityDetailTabNav
        float={true}
        onSelect={handleSelect}
        activeTab={activeSection}
        toastActive={toastActive}
      />

      <Carousel className={styles.carousel} images={images} />

      <section
        about='content'
        className={styles.content}
        ref={contentEl}
        style={{ maxHeight: getContentHeight() }}
      >
        <div
          className={`${styles.overlay_wrapper} ${
            contentActive ? styles['overlay_wrapper--inactive'] : ''
          }`}
        >
          <div className={styles.content_overlay} />
          <button
            type='button'
            className={styles.view_all}
            onClick={handleViewAllClick}
          >
            VIEW ALL
          </button>
        </div>
        <ReactMarkdown source={content} escapeHtml={false} />
      </section>

      <ActivityTimeBlock dateStart={dateStart} dateEnd={dateEnd} />
      <ActivityLocationPost location={location} />

      <div className={styles.people_info} ref={peopleEl}>
        <ActivityPeopleList
          users={userGoing}
          icon='/static/media/svg/check-outline.svg'
          activeIcon='/static/media/svg/check.svg'
          activeIconColor='#AECB4F'
          active={joined}
          text={`${userGoing.length} Going`}
          borderBottom={true}
        />
        <ActivityPeopleList
          users={userGoing}
          icon='/static/media/svg/like-outline.svg'
          activeIcon='/static/media/svg/like.svg'
          activeIconColor='#FF5C5C'
          active={liked}
          text={`${userLike.length} Like`}
        />
      </div>

      <div className={styles.comment_section} ref={commentEl}>
        <ActivityCommentList data={comments} onReplyClick={onReplyClick} />
      </div>
    </div>
  );
};

export default memo(ActivityDetailPost);
