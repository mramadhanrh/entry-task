import React, { FC, memo } from 'react';
import { sortBy, property } from 'lodash';
import styles from './index.module.scss';

import { ActivityComment, User } from '../../../interfaces';
import CommentItem from '../../molecules/CommentItem';

interface ActivityCommentListProps {
  data: ActivityComment[];
  onReplyClick?(user: User): void;
}

const ActivityCommentList: FC<ActivityCommentListProps> = ({
  data = [],
  onReplyClick = () => void 0,
}) => {
  return (
    <div className={styles.comment_list}>
      {sortBy(data, property('dateComment')).map((comment) => {
        const { id, text, user, dateComment } = comment;

        return (
          <CommentItem
            key={id}
            text={text}
            user={user}
            dateComment={dateComment}
            onReplyClick={onReplyClick}
          />
        );
      })}
    </div>
  );
};

export default memo(ActivityCommentList);
