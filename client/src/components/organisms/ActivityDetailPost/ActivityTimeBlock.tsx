import React, { FC, memo } from 'react';
import SVG from 'react-inlinesvg';
import styles from './index.module.scss';

import { getReadableDateFormat, formatAMPM } from '../../../helpers/dateHelper';
import { ActivityDateEnd } from '../../../interfaces';

interface ActivityTimeBlockProps {
  dateStart: number | string;
  dateEnd: ActivityDateEnd;
}

const ActivityTimeBlock: FC<ActivityTimeBlockProps> = ({
  dateStart = '',
  dateEnd = 'NOT_SET',
}) => {
  return (
    <section about='activity-time' className={styles.activity_time}>
      <h3 className={styles.label}>
        <div className={styles.label_decor} />
        When
      </h3>
      <div className={styles.date_wrapper}>
        <div className={`${styles.date_block} ${styles['date_block--border']}`}>
          <p>
            <span>
              <SVG src='/static/media/svg/date-from.svg' />
            </span>
            {getReadableDateFormat(new Date(dateStart), true)}
          </p>
          <div className={styles.sub_date_block}>
            <h3>
              {getReadableDateFormat(new Date(dateStart), false, true)}
              <span>{formatAMPM(new Date(dateStart)).ampm}</span>
            </h3>
          </div>
        </div>
        <div className={styles.date_block}>
          <p>
            <span>
              <SVG src='/static/media/svg/date-from.svg' />
            </span>
            {dateEnd === 'NOT_SET'
              ? 'Until Finish'
              : getReadableDateFormat(new Date(dateEnd), true)}
          </p>
          <div className={styles.sub_date_block} />
        </div>
      </div>
    </section>
  );
};

export default memo(ActivityTimeBlock);
