import React, { FC, memo } from 'react';
import GoogleMap from 'google-map-react';

import styles from './index.module.scss';
import { ActivityLocation } from '../../../interfaces';

interface ActivityLocationPostProps {
  location: ActivityLocation;
}

const ActivityLocationPost: FC<ActivityLocationPostProps> = ({
  location = {
    city: '',
    latitude: 0,
    longitude: 0,
    street: '',
  },
}) => {
  return (
    <div className={styles.location_container}>
      <h3 className={styles.label}>
        <div className={styles.label_decor} />
        When
      </h3>
      <div className={styles.location_name_wrapper}>
        <h4 className={styles.location_city}>{location.city}</h4>
        <h4 className={styles.location_address}>{location.street}</h4>
      </div>
      <div className={styles.location_map}>
        <GoogleMap
          zoom={11}
          center={{
            lat: Number(location.latitude),
            lng: Number(location.longitude),
          }}
        />
      </div>
    </div>
  );
};

export default memo(ActivityLocationPost);
