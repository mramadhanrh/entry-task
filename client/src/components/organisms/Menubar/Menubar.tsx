import React, { memo, FC } from 'react';
import { useHistory } from 'react-router-dom';
import styles from './index.module.scss';
import ButtonSvg from '../../atoms/ButtonSvg';
import { SVGAssets } from '../../../constants/assetsPath';

interface MenubarProps {
  hideSearch?: boolean;
  onSearchClick?(): void;
  profilePic?: string;
}

const Menubar: FC<MenubarProps> = ({
  hideSearch = false,
  onSearchClick = () => void 0,
  profilePic = '',
}) => {
  const history = useHistory();

  const handleHomeClick = () => {
    history.push('/');
  };

  return (
    <div className={styles.container}>
      {hideSearch ? (
        <ButtonSvg
          className={styles.search}
          src={SVGAssets.HOME}
          onClick={handleHomeClick}
        />
      ) : (
        <ButtonSvg
          className={styles.search}
          src={SVGAssets.SEARCH}
          onClick={onSearchClick}
        />
      )}

      <ButtonSvg
        className={styles.logo}
        src={SVGAssets.LOGO_CAT}
        onClick={handleHomeClick}
      />

      <button type='button' className={styles.profile}>
        <img alt='profile-pic' src={profilePic} />
      </button>
    </div>
  );
};

export default memo(Menubar);
