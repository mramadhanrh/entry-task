import React, { memo } from 'react';
import SVG from 'react-inlinesvg';
import styles from './index.module.scss';
import { SVGAssets } from '../../../constants/assetsPath';
import ToggleIcon from '../../atoms/ToggleIcon';

interface ActivityDetailFooterProps {
  joined?: boolean;
  liked?: boolean;
  onLikeClick?(): void;
  onCommentClick?(): void;
  onJoinClick?(): void;
}

const ActivityDetailFooter: React.FC<ActivityDetailFooterProps> = ({
  joined = false,
  liked = false,
  onLikeClick = () => void 0,
  onCommentClick = () => void 0,
  onJoinClick = () => void 0,
}) => {
  return (
    <div className={styles.container}>
      <div className={styles.response_wrapper}>
        <button className={styles.comment_button} onClick={onCommentClick}>
          <SVG src={SVGAssets.COMMENT_SINGLE} />
        </button>
        <button className={styles.comment_button} onClick={onLikeClick}>
          <ToggleIcon
            icon={SVGAssets.LIKE_OUTLINE}
            activeIcon={SVGAssets.LIKE}
            active={liked}
          />
        </button>
      </div>
      <div className={styles.join_wrapper}>
        <button className={styles.join_button} onClick={onJoinClick}>
          <ToggleIcon
            icon={SVGAssets.CHECK_OUTLINE}
            activeIcon={SVGAssets.CHECK}
            color='#463358'
            activeColor='#463358'
            active={joined}
          />
          <p>Join</p>
        </button>
      </div>
    </div>
  );
};

export default memo(ActivityDetailFooter);
