import React, { FC, ChangeEvent } from 'react';
import SVG from 'react-inlinesvg';

import styles from './index.module.scss';

import InputFieldIcon from '../../molecules/InputFieldIcon';
import copywriting from '../../../constants/copywriting';

interface LoginFormProps {
  title?: string;
  subTitle?: string;
  logoSrc?: string;
  emailValue?: string;
  passwordValue?: string;
  onEmailChange?(e: ChangeEvent<HTMLInputElement>): void;
  onPasswordChange?(e: ChangeEvent<HTMLInputElement>): void;
}

const LoginForm: FC<LoginFormProps> = ({
  title = copywriting.appName,
  subTitle = copywriting.loginPage.subtitle,
  logoSrc = '/static/media/svg/logo-cat.svg',
  emailValue = '',
  passwordValue = '',
  onEmailChange = () => void 0,
  onPasswordChange = () => void 0,
}) => {
  return (
    <div className={styles.container}>
      <div className={styles.wrapper}>
        <h5 className={styles.subtitle}>{subTitle}</h5>
        <h2 className={styles.title}>{title}</h2>
        <span className={styles.logo}>
          <SVG src={logoSrc} />
        </span>
      </div>

      <div className={styles.input_group}>
        <InputFieldIcon
          type='email'
          placeholder='Email'
          value={emailValue}
          onChange={onEmailChange}
        />
        <InputFieldIcon
          type='password'
          placeholder='Password'
          value={passwordValue}
          onChange={onPasswordChange}
        />
      </div>
    </div>
  );
};

export default LoginForm;
