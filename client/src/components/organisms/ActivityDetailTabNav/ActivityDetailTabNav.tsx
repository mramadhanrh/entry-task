import React, { FC, useRef, useEffect, useState, memo } from 'react';
import SVG from 'react-inlinesvg';

import styles from './index.module.scss';
import Toast from 'src/components/atoms/Toast';
import copywriting from 'src/constants/copywriting';

export type ActivityTabCategory = 'Details' | 'Participants' | 'Comments';
export type ActivityTabCategoryMap<T> = {
  [key in ActivityTabCategory]: T;
};

interface TabItem {
  text: ActivityTabCategory;
  icon: string;
  activeIcon: string;
}

interface ActivityDetailTabNavProps {
  data?: TabItem[];
  activeTab?: ActivityTabCategory;
  float?: boolean;
  toastActive?: boolean;
  onSelect?(text: ActivityTabCategory): void;
}

const defaultData: TabItem[] = [
  {
    text: 'Details',
    icon: '/static/media/svg/info-outline.svg',
    activeIcon: '/static/media/svg/info.svg',
  },
  {
    text: 'Participants',
    icon: '/static/media/svg/people-outline.svg',
    activeIcon: '/static/media/svg/people.svg',
  },
  {
    text: 'Comments',
    icon: '/static/media/svg/comment-outline.svg',
    activeIcon: '/static/media/svg/comment.svg',
  },
];

const ActivityDetailTabNav: FC<ActivityDetailTabNavProps> = ({
  data = defaultData,
  activeTab = 'Details',
  float = false,
  toastActive = false,
  onSelect = () => void 0,
}) => {
  const [isFloat, setFloat] = useState(false);
  const persistEl = useRef<HTMLDivElement>(null);
  const floatEl = useRef<HTMLDivElement>(null);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleFloatOnScroll = () => {
    if (!floatEl.current || !persistEl.current) return;

    const { offsetTop, offsetHeight } = persistEl.current;
    const isOverElement =
      document.documentElement.scrollTop > offsetTop - offsetHeight;

    if (isOverElement !== isFloat) {
      setFloat(isOverElement);
    }
  };

  useEffect(() => {
    if (float) window.addEventListener('scroll', handleFloatOnScroll);

    return () => {
      if (float) window.removeEventListener('scroll', handleFloatOnScroll);
    };
  }, [handleFloatOnScroll, float]);

  return (
    <div className={styles.container} ref={persistEl}>
      <div
        className={`${isFloat ? styles['container_float--active'] : ''}`}
        ref={floatEl}
      >
        <div className={`${styles.float_tab}`}>
          {data.map((item, idx) => {
            return (
              <button
                key={item.text}
                type='button'
                className={`${styles.tab_item} ${
                  activeTab === item.text ? styles['tab_item--active'] : ''
                }`}
                onClick={() => onSelect(item.text)}
              >
                <div className={idx !== 0 ? styles['tab_item--border'] : ''}>
                  <SVG
                    src={`${
                      activeTab === item.text ? item.activeIcon : item.icon
                    }`}
                  />
                  <p>{item.text}</p>
                </div>
              </button>
            );
          })}
        </div>

        <Toast
          className={styles.toast}
          text={copywriting.comment.newMessage}
          active={toastActive}
        />
      </div>
    </div>
  );
};

export default memo(ActivityDetailTabNav);
