import React, { FC } from 'react';
import styles from './index.module.scss';

import CategoryTitle from '../../atoms/CategoryTitle';
import CategoryBadge from '../../atoms/CategoryBadge';

interface CategoryListProps {
  className?: string;
  itemClassname?: string;
  title?: string;
  data: string[];
  activeIndex?: number | null;
  onSelect(value: number): void;
}

type CategoryBadgeProps = React.ComponentProps<typeof CategoryBadge>;

type OmittedCategoryBadgeProps = Omit<CategoryBadgeProps, 'onClick'>;

const CategoryList: FC<CategoryListProps & OmittedCategoryBadgeProps> = ({
  className = '',
  itemClassname = '',
  title = '',
  data = [],
  activeIndex = null,
  onSelect = () => void 0,
  ...props
}) => {
  const handleSelect = (index: number) => {
    onSelect(index);
  };

  return (
    <div className={`${styles.container} ${className}`}>
      <CategoryTitle className={styles.title} text={title} />

      <div className={styles.date_collection}>
        {data.map((dateName, idx) => {
          return (
            <CategoryBadge
              key={idx.toString()}
              {...props}
              className={itemClassname}
              text={dateName}
              active={activeIndex === idx}
              onClick={() => handleSelect(idx)}
            />
          );
        })}
      </div>
    </div>
  );
};

export default CategoryList;
