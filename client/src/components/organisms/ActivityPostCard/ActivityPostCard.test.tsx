import React from 'react';
import { shallow } from 'enzyme';
import ActivityPostCard from './ActivityPostCard';

jest.mock('react-router-dom', () => ({
  useHistory: () => ({
    push: jest.fn(),
  }),
}));

describe('ActivityPostCard', () => {
  it('Render without any data', () => {
    const card = shallow(<ActivityPostCard />);
    expect(card).toMatchSnapshot();
  });

  it('Render with all props', () => {
    const card = shallow(
      <ActivityPostCard
        href='/'
        title='Lorem Ipsum'
        description='Lorem Ipsum Dolor sit amet'
        postImg='http://example.com'
        postImgAlt='img-sample'
        author={{
          id: 0,
          fullname: 'John doe',
          nickname: 'johndoe',
          picture: 'http://example.com',
        }}
        dateStart={0}
        dateEnd={0}
        likeCount={10}
        goingCount={10}
        going={true}
        liked={true}
        onLikeClick={() => void 0}
        onGoingClick={() => void 0}
      />
    );
    expect(card).toMatchSnapshot();
  });
});
