import React, { useMemo, FC, memo } from 'react';
import ReactMarkdown from 'react-markdown';
import { useHistory } from 'react-router-dom';
import styles from './index.module.scss';

import AvatarUsername from '../../molecules/AvatarUsername';
import ChannelBadge from '../../atoms/ChannelBadge';
import DatePostBadge from '../../molecules/DatePostBadge';
import { limitString, escapeHtml } from '../../../helpers/stringHelper';
import ToggleIconLabel from '../../molecules/ToggleIconLabel';
import { User, ActivityDateEnd } from '../../../interfaces';

interface ActivityPostCardProps {
  href?: string;
  title?: string;
  description?: string;
  postImg?: string;
  postImgAlt?: string;
  author?: User;
  dateStart?: number | string;
  dateEnd?: ActivityDateEnd;
  channelName?: string;
  likeCount?: number;
  goingCount?: number;
  going?: boolean;
  liked?: boolean;
  onLikeClick?(value: boolean): void;
  onGoingClick?(value: boolean): void;
}

const ActivityPostCard: FC<ActivityPostCardProps> = ({
  href = '#',
  title = '',
  description = '',
  postImg = '',
  postImgAlt = '',
  author = {
    id: 0,
    fullname: '',
    nickname: '',
    picture: '',
  },
  channelName = '',
  dateStart = 'Unknown',
  dateEnd = 'NOT_SET' as ActivityDateEnd,
  likeCount = 0,
  goingCount = 0,
  going = false,
  liked = false,
  onLikeClick = () => void 0,
  onGoingClick = () => void 0,
}) => {
  const history = useHistory();

  const handleLikeClick = () => {
    onLikeClick(!liked);
  };

  const handleGoingClick = () => {
    onGoingClick(!going);
  };

  const handleClick = () => {
    history.push(href);
  };

  const descriptionMemo = useMemo(
    () => limitString(escapeHtml(description), 300, true),
    [description]
  );

  return (
    <div className={styles.container}>
      <div className={styles.info}>
        <AvatarUsername name={author.fullname} src={author.picture} />
        <ChannelBadge name={channelName} />
      </div>

      <div className={styles.post_wrapper}>
        <div className={styles.post_info_wrapper}>
          <button onClick={handleClick}>
            <div className={styles.header}>
              <h3 className={styles.title}>{title}</h3>
              <DatePostBadge timestart={dateStart} timeend={dateEnd} />
            </div>

            <ReactMarkdown
              className={styles.description}
              source={descriptionMemo}
              escapeHtml={true}
            />
          </button>

          <div className={styles.post_action}>
            <ToggleIconLabel
              icon='/static/media/svg/like-outline.svg'
              activeIcon='/static/media/svg/like.svg'
              activeIconColor='#FF5C5C'
              text={`${likeCount} Likes`}
              activeText='I like it'
              active={liked}
              onClick={handleLikeClick}
            />
            <ToggleIconLabel
              icon='/static/media/svg/check-outline.svg'
              activeIcon='/static/media/svg/check.svg'
              activeIconColor='#AECB4F'
              text={`${goingCount} Going`}
              activeText='I am going!'
              onClick={handleGoingClick}
              active={going}
            />
          </div>
        </div>

        <img className={styles.post_img} alt={postImgAlt} src={postImg} />
      </div>
    </div>
  );
};

export default memo(ActivityPostCard);
