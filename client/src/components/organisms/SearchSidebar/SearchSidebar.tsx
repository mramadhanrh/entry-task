import React, { useState, FC, useMemo, memo } from 'react';
import SVG from 'react-inlinesvg';
import styles from './index.module.scss';
import CategoryList from '../CategoryList';
import DateCategoryList from '../DateCategoryList';
import { DateCategoryResponse } from '../DateCategoryList/DateCategoryList';
import FullContainerButton from '../../atoms/FullContainerButton';
import { getCopywritingDateRange } from '../../../helpers/dateHelper';

interface SearchSidebarProps {
  channelData: string[];
  onSearchClick?(value: DateCategoryResponse, channel: string): void;
}

const SearchSidebar: FC<SearchSidebarProps> = ({
  channelData = [],
  onSearchClick = () => void 0,
}) => {
  const [activeChannelIndex, setActiveChannelIndex] = useState<number | null>(
    null
  );
  const [dateData, setDateData] = useState<DateCategoryResponse>({
    category: undefined,
    from: undefined,
    to: undefined,
  });
  const channelDataMemo = useMemo(() => ['All', ...channelData], [channelData]);

  const getChannelActivitiesText = () => {
    if (activeChannelIndex === null) return '';
    if (channelDataMemo[activeChannelIndex] === 'All') return 'All activities';

    return `${channelDataMemo[activeChannelIndex]} activities`;
  };

  const getDateText = () => {
    if (dateData.category === 'anytime') return '';
    if (dateData.category === 'later') {
      return getCopywritingDateRange(dateData.from, dateData.to);
    }
    if (dateData.category) return `for ${dateData.category}`;
    return '';
  };

  const handleDateChange = (e: DateCategoryResponse) => {
    setDateData(e);
  };

  const handleChannelSelect = (value: number) => {
    setActiveChannelIndex(value);
  };

  const handleSearchClick = () => {
    if (activeChannelIndex !== null) {
      onSearchClick(dateData, channelDataMemo[activeChannelIndex]);
    }
  };

  const isSearchDisabled =
    dateData.category === undefined || activeChannelIndex === null;

  return (
    <div className={styles.container}>
      <div className={styles.content_wrapper}>
        <div>
          <DateCategoryList onDateChange={handleDateChange} />
        </div>
        <div>
          <CategoryList
            title='CHANNEL'
            data={channelDataMemo}
            itemClassname={styles.channel_item}
            activeIndex={activeChannelIndex}
            onSelect={handleChannelSelect}
          />
        </div>
      </div>

      <div className={styles.search_button_wrapper}>
        <FullContainerButton
          className={styles.search_btn}
          onClick={handleSearchClick}
          disabled={isSearchDisabled}
        >
          <div className={styles.search_text}>
            <span>
              <SVG src='/static/media/svg/search.svg' />
            </span>
            <p>SEARCH</p>
          </div>
          <p
            className={styles.search_details}
          >{`${getChannelActivitiesText()} ${getDateText()}`}</p>
        </FullContainerButton>
      </div>
    </div>
  );
};

export default memo(SearchSidebar);
