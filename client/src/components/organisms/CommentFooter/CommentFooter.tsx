import React, {
  FC,
  FormEvent,
  useEffect,
  useRef,
  useState,
  ChangeEvent,
  memo,
} from 'react';
import SVG from 'react-inlinesvg';

import styles from './index.module.scss';

import { SVGAssets } from '../../../constants/assetsPath';
import copywriting from '../../../constants/copywriting';

interface CommentFooterProps {
  mention?: string;
  onSendComment?(value: string): void;
  onClose?(): void;
}

const CommentFooter: FC<CommentFooterProps> = ({
  mention = '',
  onSendComment = () => void 0,
  onClose = () => void 0,
}) => {
  const inputEl = useRef<HTMLInputElement>(null);
  const [value, setValue] = useState<string>('');

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value: inputVal },
    } = e;
    setValue(inputVal);
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (value.trim() !== '') {
      onSendComment(value);
      setValue('');
    }
  };

  useEffect(() => {
    if (mention) setValue(`@${mention} `);
    inputEl.current?.focus();
  }, [mention]);

  return (
    <form onSubmit={handleSubmit} className={styles.container}>
      <div className={styles.field_wrapper}>
        <button type='button' className={styles.close_btn} onClick={onClose}>
          <SVG src={SVGAssets.CROSS} />
        </button>
        <input
          ref={inputEl}
          className={styles.comment_field}
          placeholder={copywriting.comment.placeholder}
          value={value}
          onChange={handleChange}
        />
      </div>
      <div className={styles.send_wrapper}>
        <button className={styles.send_btn} type='submit'>
          <SVG src={SVGAssets.SEND} />
        </button>
      </div>
    </form>
  );
};

export default memo(CommentFooter);
