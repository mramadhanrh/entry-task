import React, { useState, FC, ChangeEvent, memo } from 'react';
import styles from './index.module.scss';

import CategoryList from '../CategoryList/CategoryList';
import CategoryBadge from '../../atoms/CategoryBadge';
import DateRangePicker from '../../molecules/DateRangePicker';
import { getDateRange, getCategoryRange } from '../../../helpers/dateHelper';

export type DateCategory =
  | 'anytime'
  | 'today'
  | 'tomorrow'
  | 'this week'
  | 'this month'
  | 'later';

export interface DateCategoryResponse {
  category?: DateCategory;
  from?: number;
  to?: number;
}

type DateCategoryChangeEvent = (param: DateCategoryResponse) => void;

interface DateCategoryListProps {
  title?: string;
  data?: DateCategory[];
  onDateChange?: DateCategoryChangeEvent;
}

const dateCategoryData = [
  'anytime',
  'today',
  'tomorrow',
  'this week',
  'this month',
] as DateCategory[];

const DateCategoryList: FC<DateCategoryListProps> = ({
  title = '',
  data = dateCategoryData,
  onDateChange = () => void 0,
}) => {
  const [activeIndex, setActiveIndex] = useState<null | number>(null);
  const [customFrom, setCustomFrom] = useState<string>('');
  const [customTo, setCustomTo] = useState<string>('');
  const datePickerIndex = data.length;

  const getDateRangeByCategory = (
    category: DateCategory,
    from?: number,
    to?: number
  ): DateCategoryResponse => {
    if (category === 'anytime') return { category };
    if (category === 'later') {
      return {
        category,
        from: from || (customFrom ? +new Date(customFrom) : undefined),
        to: to || (customTo ? +new Date(customTo) : undefined),
      };
    }

    const { from: crFrom, by } = getCategoryRange(category);
    const { from: rFrom, to: rTo } = getDateRange(crFrom, by);
    return {
      category,
      from: +rFrom,
      to: +rTo,
    };
  };

  const handleSelect = (index: number) => {
    setActiveIndex(index);
    onDateChange(getDateRangeByCategory(data[index]));
  };

  const handleLaterFromChange = (e: ChangeEvent<HTMLInputElement>) => {
    setCustomFrom(e.target.value);
    onDateChange(getDateRangeByCategory('later', +new Date(e.target.value)));
  };

  const handleLaterToChange = (e: ChangeEvent<HTMLInputElement>) => {
    setCustomTo(e.target.value);
    onDateChange(
      getDateRangeByCategory('later', undefined, +new Date(e.target.value))
    );
  };

  return (
    <>
      <CategoryList
        title={title}
        data={data}
        activeIndex={activeIndex}
        itemClassname={styles.list_item}
        uppercase={true}
        borderless={true}
        onSelect={handleSelect}
      />
      <CategoryBadge
        className={styles.list_item}
        text='later'
        uppercase={true}
        borderless={true}
        active={activeIndex === datePickerIndex}
        onClick={() => handleSelect(datePickerIndex)}
      />
      <div
        id='date-range'
        className={`${styles.date_picker} ${
          activeIndex === datePickerIndex ? styles['date_picker--active'] : ''
        }`}
      >
        <DateRangePicker
          valueFrom={customFrom}
          valueTo={customTo}
          onFromChange={handleLaterFromChange}
          onToChange={handleLaterToChange}
        />
      </div>
    </>
  );
};

export default memo(DateCategoryList);
