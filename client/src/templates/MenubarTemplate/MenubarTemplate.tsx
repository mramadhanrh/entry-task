import React, { FC } from 'react';
import styles from './index.module.scss';

interface MenubarTemplateProps {
  header?: React.ReactNode;
  sider?: React.ReactNode;
  footer?: React.ReactNode;
  children?: React.ReactNode;
  siderActive?: boolean;
  onSiderCancel?(): void;
}

const MenubarTemplate: FC<MenubarTemplateProps> = ({
  header,
  sider,
  footer,
  children,
  siderActive = false,
  onSiderCancel = () => void 0,
}) => {
  const siderSlideStyle = siderActive ? styles.sider_active : '';

  return (
    <div className={`${styles.container} ${siderSlideStyle}`}>
      <aside>
        <button
          type='button'
          className={styles.overlay}
          onClick={onSiderCancel}
        >
          <div />
        </button>
        <div className={styles.foreground}>{sider}</div>
      </aside>

      <header>{header}</header>
      <main>{children}</main>
      {footer && (
        <>
          <div className={styles.footer_gap} />
          <footer>{footer}</footer>
        </>
      )}
    </div>
  );
};

export default MenubarTemplate;
