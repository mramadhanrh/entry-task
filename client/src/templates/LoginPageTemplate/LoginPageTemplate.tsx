import React, { FC } from 'react';
import styles from './index.module.scss';

interface LoginPageTemplateProps {
  children?: React.ReactNode;
  footer?: React.ReactNode;
  background?: string;
}

const LoginPageTemplate: FC<LoginPageTemplateProps> = ({
  children,
  footer,
  background = '/static/media/img/Street-Dance-01.jpg',
}: LoginPageTemplateProps) => {
  return (
    <div className={styles.container}>
      <main style={{ backgroundImage: `url(${background})` }}>
        <div className={styles.layer} />
        <div className={styles.content}>{children}</div>
      </main>
      <footer>{footer}</footer>
    </div>
  );
};

export default LoginPageTemplate;
